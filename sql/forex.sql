-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2016 at 03:22 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forex`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `privilege` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `password`, `privilege`, `token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'aji', 'asdasdasd', 'super', NULL, '2016-04-23 09:11:54', '2016-04-23 09:11:54', NULL),
(13, 'ajo', '$2y$10$/r1U04cXZJdJ60foYzfX5uNFi37Xx.VIlHWTA5DTBp3wBD2IEzXbm', 'super', 'PGXYOvtmJZ', '2016-04-23 13:19:59', '2016-04-23 13:34:56', NULL),
(15, 'joko', '$2y$10$CIScTKbwxYIKScI6sG9amOxrgqf38hPkJxIUVKKHurdfGRQvx3QR2', 'super', NULL, '2016-04-23 13:35:06', '2016-04-23 13:45:00', '2016-04-23 13:45:00'),
(16, 'joka', '$2y$10$US9Hw6QQSh/cOtWPhSKou.6L4QXHFPhsoeIUGdyrnbcEVY24ORsyG', 'super', NULL, '2016-04-23 13:35:31', '2016-04-23 13:43:54', '2016-04-23 13:43:54'),
(17, 'joko', '$2y$10$gmgfsWPMF0kT9mSkGkpjxOV4GiqSH9gTJYLO32LqD9JW9rfBB3uu6', 'super', NULL, '2016-04-23 13:48:33', '2016-04-23 13:48:33', NULL),
(18, 'jokaw', '$2y$10$8LZz6Xyycd0nbx8QsqKrCOWq/tTvVJL5bdx765if3FqqKNetFvEw.', 'super', NULL, '2016-04-23 15:27:26', '2016-04-23 15:27:26', NULL),
(19, 'joki', '$2y$10$lnsh4vZKwbLKycqtEyySFen/YHlPTxXqghdBRA2IL/JtyCsx6b3oG', 'admin', '7tQYtrYmZw', '2016-04-23 15:27:37', '2016-04-23 15:27:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endSubscribe` timestamp NULL DEFAULT NULL,
  `gcmId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `password`, `email`, `country`, `phone`, `registered`, `endSubscribe`, `gcmId`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ajo', 'asdasdasd', 'aji@gmail.com', 'indo', '0123810283', '2016-04-23 15:07:16', NULL, '', '2016-04-23 08:07:16', '2016-04-23 16:28:17', '2016-04-23 16:28:17'),
(3, 'aji', 'aji123', 'ajo@gmail.com', 'indo', '0123810283', '2016-04-23 15:19:15', NULL, 'kjsadgbfasdf981273712njhsaf', '2016-04-23 08:19:15', '2016-04-23 08:19:15', NULL),
(4, 'aji', 'aji123', 'aja@gmail.com', 'indo', '0123810283', '2016-04-23 15:19:41', NULL, 'kjsadgbfasdf981273712njhsaf', '2016-04-23 08:19:41', '2016-04-23 08:19:41', NULL),
(5, 'aji', 'aji123', 'aje@gmail.com', 'indo', '0123810283', '2016-04-23 15:32:29', '2016-04-21 17:00:00', 'asdasdasdsadas', '2016-04-23 08:32:29', '2016-04-23 09:31:10', NULL),
(6, 'aji', 'aji123', 'ajee@gmail.com', 'indo', '0123810283', '2016-04-23 17:48:49', NULL, 'kjsadgbfasdf981273712njhsaf', '2016-04-23 10:48:49', '2016-04-23 10:48:49', NULL),
(7, 'aji', 'aji123', 'ajasd@gmail.com', 'indo', '0123810283', '2016-04-23 18:00:16', '2017-08-22 17:00:00', 'kjsadgbfasdf981273712njhsaf', '2016-04-23 11:00:16', '2016-04-23 17:15:25', NULL),
(8, 'aji', '$2y$10$8yEMPeHwddXv4P4o2tnr0eVAh2.Bcri/t7oS3YgGa50cIcWXEBmze', 'asdasd@gmail.com', 'indo', '0123810283', '2016-04-23 18:09:35', '2016-06-22 17:00:00', 'kjsadgbfasdf981273712njhsaf', '2016-04-23 11:09:35', '2016-04-23 15:38:13', '2016-04-23 15:38:13');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phone`, `email`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '13123', '112@gmail.com', 'asdsbdaj', '2016-04-23 15:43:33', '2016-04-23 15:45:37', '2016-04-23 15:45:37'),
(2, '124151521', '112@gmail.com', 'asdsbdaj', '2016-04-23 15:49:32', '2016-04-23 15:51:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_04_23_001454_create_admins_table', 1),
('2016_04_23_002542_create_clients_table', 2),
('2016_04_23_002703_create_contacts_table', 3),
('2016_04_23_002721_create_tutorials_table', 4),
('2016_04_23_002937_create_tutorials_table', 5),
('2016_04_23_003527_create_signals_table', 6),
('2016_04_23_123431_create_t1s_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `signals`
--

CREATE TABLE `signals` (
  `id` int(10) UNSIGNED NOT NULL,
  `published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `currencyPair` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dailyTrend` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entryLevel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TP1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TP2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TP3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `triggeredAt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `closedAt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `signals`
--

INSERT INTO `signals` (`id`, `published`, `currencyPair`, `dailyTrend`, `mode`, `entryLevel`, `TP1`, `TP2`, `TP3`, `SL`, `note`, `triggeredAt`, `closedAt`, `profit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, '2016-04-23 20:30:47', 'Rupiah/Dollar', 'Up', 'Buy', '0.0009', '123', '123', '123', '12312', 'ok ga', '', '', '', '2016-04-23 13:30:47', '2016-04-23 13:30:47', NULL),
(4, '2016-04-23 20:31:59', 'Rupiah/Dollar', 'Up', 'Buy', '0.0009', '123', '123', '123', '12312', 'ok kan??', '12309123', '12312321', '', '2016-04-23 13:31:59', '2016-04-23 14:52:43', '2016-04-23 14:52:43'),
(5, '2016-04-23 21:50:36', 'Rupiah/Dollar', 'Up', 'Buy', '0.0009', '123', '123', '123', '12312', 'ok ga', '12309123', '', '', '2016-04-23 14:50:36', '2016-04-23 14:53:53', '2016-04-23 14:53:53'),
(6, '2016-04-23 21:51:05', 'Rupiah/Dollar', 'Up', 'Buy', '0.0009', '123', '123', '123', '12312', 'ok ga', '12309123', '12312321', '', '2016-04-23 14:51:05', '2016-04-23 14:55:47', '2016-04-23 14:55:47'),
(7, '2016-04-23 22:07:15', 'Rupiah/Dollar', 'Up', 'Buy', '0.0009', '123', '123', '123', '12312', 'ok ga', '12309123', '12312321', '', '2016-04-23 15:07:15', '2016-04-23 15:07:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t1s`
--

CREATE TABLE `t1s` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t1s`
--

INSERT INTO `t1s` (`id`, `name`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ajo', 'aji123', '2016-04-23 06:50:35', '2016-04-23 06:50:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE `tutorials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `writen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tutorials`
--

INSERT INTO `tutorials` (`id`, `title`, `content`, `image`, `thumbnail`, `author`, `writen`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'manuju kesampatan', 'asdasda', 'asdasdasd', 'asdfff', 'asdasdad', '2016-04-23 23:03:46', '2016-04-23 16:03:46', '2016-04-23 16:10:56', '2016-04-23 16:10:56'),
(2, 'manuju kesampatan', 'asdasda', 'asdasdasd', 'asdasdasd', 'asdasdad', '2016-04-23 23:05:12', '2016-04-23 16:05:12', '2016-04-23 16:05:12', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signals`
--
ALTER TABLE `signals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t1s`
--
ALTER TABLE `t1s`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `signals`
--
ALTER TABLE `signals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t1s`
--
ALTER TABLE `t1s`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tutorials`
--
ALTER TABLE `tutorials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
