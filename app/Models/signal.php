<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="signal",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="published",
 *          description="published",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="currencyPair",
 *          description="currencyPair",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="dailyTrend",
 *          description="dailyTrend",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mode",
 *          description="mode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="entryLevel",
 *          description="entryLevel",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="TP1",
 *          description="TP1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="TP2",
 *          description="TP2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="TP3",
 *          description="TP3",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="SL",
 *          description="SL",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="triggeredAt",
 *          description="triggeredAt",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="closedAt",
 *          description="closedAt",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="profit",
 *          description="profit",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class signal extends Model
{
    use SoftDeletes;

    public $table = 'signals';
    

    protected $dates = ['deleted_at','published'];


    public $fillable = [
        'published',
        'currencyPair',
        'dailyTrend',
        'mode',
        'entryLevel',
        'TP1',
        'TP2',
        'TP3',
        'SL',
        'note',
        'triggeredAt',
        'closedAt',
        'profit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'date-time',
        'currencyPair' => 'string',
        'dailyTrend' => 'string',
        'mode' => 'string',
        'entryLevel' => 'string',
        'TP1' => 'string',
        'TP2' => 'string',
        'TP3' => 'string',
        'SL' => 'string',
        'note' => 'string',
        'triggeredAt' => 'string',
        'closedAt' => 'string',
        'profit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
