<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/






Route::resource('admins', 'adminAPIController');

Route::resource('clients', 'clientAPIController');

Route::resource('contacts', 'contactAPIController');

Route::resource('tutorials', 'tutorialAPIController');

Route::resource('signals', 'signalAPIController');

Route::resource('t1s', 't1APIController');

Route::resource('api','APICOntroller');

Route::resource('notes', 'noteAPIController');

Route::resource('versions', 'VersionAPIController');