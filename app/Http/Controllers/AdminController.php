<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon\Carbon;

use App\Http\Requests\API\CreateadminAPIRequest;
use App\Http\Requests\API\UpdateadminAPIRequest;
use App\Models\admin;
use App\Repositories\adminRepository;
use App\Models\signal;
use App\Repositories\signalRepository;
use App\Models\client;
use App\Repositories\clientRepository;
use App\Models\contact;
use App\Repositories\contactRepository;

use App\Models\Version;
use App\Repositories\VersionRepository;
use App\Models\Tutorial;
use App\Repositories\tutorialRepository;

use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Hash;
use Session;
use DB;


class AdminController extends Controller

{
    private $adminRepository;
    private $signalRepository;
    private $clientRepository;
    private $contactRepository;
    private $tutorialRepository;

    public function __construct(adminRepository $adminRepo, signalRepository $signalRepo,clientRepository $clientRepo,contactRepository $contactRepo,tutorialRepository $tutorialRepo,VersionRepository $versionRepo)
    {
       
        $this->adminRepository = $adminRepo;
        $this->signalRepository = $signalRepo;
        $this->clientRepository = $clientRepo;
        $this->contactRepository = $contactRepo;
        $this->tutorialRepository= $tutorialRepo;
        $this->versionRepository = $versionRepo;
    }

    //fungsi LOGIN
    public function login(){
        
        
        return view('admin.login.signin',compact('message','isLogin'));
    }

    public function signout(){
        
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        Session::flush();
        return redirect('admin/login');
    }

    public function signin(CreateadminAPIRequest $request){

       
        $isLogin = true;
        $input = $request->only('name','password');
        $input['token'] = $this->generateRandomString();
        $token = $input['token'];
        $name = $input['name'];
        /** @var admin $admin */
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        $privilege = $admin[0]["privilege"];
        // var_dump($admin[0][]);die;
        if (!empty($admin)) {
            if (Hash::check($input['password'], $admin[0]["password"])){
                $input['password'] = Hash::make($input['password']);
                $admin = $this->adminRepository->update($input,$admin[0]['id']);
                Session::put(['name' => $name,'token' => $token,'privilege' => $privilege]);
                // session(['name' => $name,'token' => $token,'privilege' => $admin[0]["privilege"]]);
                return redirect('admin');
                
            }
            else{
                $isLogin = 'false';
                $message = 'password not match';
                return redirect()->back()->withErrors($message);
                // return redirect()->with('message',$message)->with('isLogin',$isLogin)->back();
                
            }
            
        }
        else{
            $isLogin = 'false';
            $message = 'username not found';
            return redirect()->back()->withErrors($message);
            // return redirect()->with('message',$message)->with('isLogin',$isLogin)->back();
            
        }
    }

    // fungsi ADMIN
    public function dashboard(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }

        $token = Session::get('token');
        $name = Session::get('name');
        $privilege = Session::get('privilege');
        
        if($privilege=="super"){

            $admin = $this->adminRepository->orderBy('privilege','desc')->all()->toArray();

        }else{
            
            $admin = $this->adminRepository->findByField('name',$name)->toArray();
        }
        // echo "<pre>";print_r($admin);echo "</pre>";die;
    	return view('admin.dashboard.list',compact('admin'));

    }

    public function addAdmin(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }

        return view('admin.dashboard.add');

    }

    public function setAdmin(Request $request){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        // $name = $request->input('name');
        // echo $name;die;

        $token = Session::get('token');
        $name = Session::get('name');
        $username = strtolower($request->input('name'));
        $password = strtolower($request->input('password'));
        $privilege = strtolower($request->input('privilege'));
       
        // dd($request);die;
        // $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$name)->toArray();
        
        if (!empty($admin)) {
            if ($token == $admin[0]["token"]  && $admin[0]['privilege'] == "super"){
                // $input = $request->only('data')['data'];
                $password = Hash::make($password);
                $input = array("name"=>$username,"password"=> $password,"privilege"=>$privilege);
                $admin = $this->adminRepository->findByField('name',$username)->toArray();
                if (empty($admin)) {
                    $this->adminRepository->create($input);
                    return redirect('admin')->withErrors('Username has created Succesfully');
                    // return $this->sendResponse($admins->toArray(), 'admin saved successfully');
                }
                else{
                    return redirect('admin/dashboard/add')->withErrors('Username has already existed');
                    // return Response::json(ResponseUtil::makeError('admin exist'), 400);    
                }
            }
            else{
                Session::flush();
                return redirect('admin/login');   
                // return redirect('admin')->withErrors('username or privilege has unauthorized');
                // return Response::json(ResponseUtil::makeError('unauthorized'), 400);
            }
            
        }
        else{
            Session::flush();
            return redirect('admin/login');    
        }
    }

    public function editAdmin($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $admin = $this->adminRepository->find($id)->toArray();
        
        return view('admin.dashboard.edit',compact('admin'));

    }

    public function updateAdmin($id,Request $request){

        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }



        $token = Session::get('token');
        $name = Session::get('name');
        $username = strtolower($request->input('name'));
        $oldpassword = strtolower($request->input('old_password'));
        // $oldpassword = Hash::make($oldpassword);
        $newpassword = strtolower($request->input('password'));
        $newpassword = Hash::make($newpassword);
        $privilege = $request->input('privilege');

        $cekdata = $this->adminRepository->find($id)->toArray();
        
        $admin = $this->adminRepository->findByField('name',$name)->toArray();

        if(!empty($admin)){
            if($token == $admin[0]['token'] ){
                if(Hash::check($oldpassword, $cekdata['password'])){
                    
                    $cekName = $this->adminRepository->findByField('name',$username)->toArray();
                    if(empty($cekName) || $username==$cekdata['name']){
                         $input = array("name"=>$username,"password"=> $newpassword,"privilege"=>$privilege);
                         $this->adminRepository->update($input,$id);
                         return redirect('admin')->withErrors('Username has updated Succesfully');
                     }else{


                        return redirect('admin/dashboard/edit/'.$id)->withErrors('Username has already existed');
                     }
                   
                }else{
                    return redirect('admin/dashboard/edit/'.$id)->withErrors('Old password not match');

                }

            }else{

               Session::flush();
               return redirect('admin/login');   
            }
                

        }else{
            return redirect('admin')->withErrors('error updating');
        }
            

    }

    public function deleteAdmin($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $token = Session::get('token');
        $name = Session::get('name');
        $admin = $this->adminRepository->findByField('name',$name)->toArray();
        
        
        if(!empty($admin)){
            if($token == $admin[0]["token"]){
                if($admin[0]['privilege'] == "super"){
                    $dataAdmin = $this->adminRepository->find($id);
                    $dataAdmin->delete();
                    // $this->adminRepository->delete($id);
                    return redirect()->back()->withErrors('Username has deleted Succesfully');
                    
                    
                   
                }else{
                    return redirect()->back()->withErrors('only super admin can delete username. contact your super admin.');

                }

            }else{

               Session::flush();
               return redirect('admin/login');   
            }
                

        }else{
            return redirect()->back()->withErrors('error deleting');
        }

    }

    // fungsi USER

    public function activeUser(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        
        $result = $this->clientRepository->all()->toArray();
        return view('admin.user.active',compact('result'));

    }

    public function detailUser($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->clientRepository->find($id)->toArray();
        return view('admin.user.detail',compact('result'));

    }

    public function deleteUser($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $token = Session::get('token');
        $name = Session::get('name');
        $admin = $this->adminRepository->findByField('name',$name)->toArray();
        
        if(!empty($admin)){
            if($token == $admin[0]["token"]){
                if($admin[0]['privilege'] == "super"){
                    $dataAdmin = $this->clientRepository->find($id);
                    $dataAdmin->delete();
                    // $this->adminRepository->delete($id);
                    return redirect()->back()->withErrors('User has banned Succesfully');
                    
                    
                   
                }else{
                    return redirect()->back()->withErrors('only super admin can ban user. contact your super admin.');

                }

            }else{

               Session::flush();
               return redirect('admin/login');   
            }
                

        }else{
            return redirect()->back()->withErrors('error deleting');
        }
        
    }

    public function bannedUser(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->clientRepository->onlyTrashed()->all();
       
        return view('admin.user.banned',compact('result'));

    }

    public function detailBannedUser($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->clientRepository->withTrashed()->find($id)->toArray();
        return view('admin.user.detail',compact('result'));
    }

     public function restoreUser($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $token = Session::get('token');
        $name = Session::get('name');
        $admin = $this->adminRepository->findByField('name',$name)->toArray();

        if(!empty($admin)){
            if($token == $admin[0]["token"]){
                if($admin[0]['privilege'] == "super"){
                    $dataAdmin = $this->clientRepository->withTrashed()->find($id);
                    $dataAdmin->restore();
                    // $this->adminRepository->delete($id);
                    return redirect()->back()->withErrors('User has restored Succesfully');
                    
                    
                   
                }else{
                    return redirect()->back()->withErrors('only super admin can restore user. contact your super admin.');

                }

            }else{

               Session::flush();
               return redirect('admin/login');   
            }
                

        }else{
            return redirect()->back()->withErrors('error restoring');
        }


        
    }

   

    // fungsi signal/stat

    public function stat(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->signalRepository->orderBy('published','desc')->all()->toArray();
        return view('admin.stat.list',compact('result'));

    }

    public function addStat(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        return view('admin.stat.add');

    }

    public function deleteStat($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }

        $token = Session::get('token');
        $name = Session::get('name');
        $admin = $this->adminRepository->findByField('name',$name)->toArray();
        
        
        if(!empty($admin)){
            if($token == $admin[0]["token"]){
                $datastat = $this->signalRepository->find($id);
                $datastat->delete();
                
                return redirect()->back()->withErrors('Signal has deleted Succesfully');
              
            }else{

               Session::flush();
               return redirect('admin/login');   
            }
                

        }else{
            return redirect()->back()->withErrors('error deleting');
        }



        

    }


    public function editStat($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->signalRepository->find($id)->toArray();
        // print_r($result);die;
        return view('admin.stat.edit',compact('result'));

    }

    public function updateStat($id, Request $request){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        
        // var_dump($request->all());
        // die;
        $token = Session::get('token');
        $name = Session::get('name');
       
        $admin = $this->adminRepository->findByField('name', $name )->toArray();
        if (!empty($admin)) {
            if ($token == $admin[0]["token"]){
                $input = $request->all();
                $signal = $this->signalRepository->update($input, $id);
                return redirect('admin/stat')->withErrors('Signal has updated Succesfully');
            }
            else{
                return redirect('admin/stat/edit/'.$id)->withErrors('error updating signal. please try again!');
            }
            
        }
        else{
           Session::flush();
           return redirect('admin/login');    
        }


      

    }

    // fungsi contact

    public function contact(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->contactRepository->all()->toArray();
        // var_dump($result);die;
        return view('admin.contact.contact',compact('result'));

    }

    public function updateContact(Request $request){

        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        
        // var_dump($request->all());
        // die;
        $token = Session::get('token');
        $name = Session::get('name');
       
        $admin = $this->adminRepository->findByField('name', $name )->toArray();
        if (!empty($admin)) {
            if ($token == $admin[0]["token"]){
                $input = $request->all();
                // print_r($input);die;
                $input = array("facebookLink"=>$input['facebook'],"twitterLink"=> $input['twitter'],"googleLink"=>$input['google']);
                $id=1;
                $contact = $this->contactRepository->update($input, $id);
               
                return redirect('admin/contact')->with('message','contact has updated Succesfully');
            }
            else{
                return redirect('admin/contact')->withErrors('error updating contact. please try again!');
            }
            
        }
        else{
           Session::flush();
           return redirect('admin/login');    
        }

    } 

    //fungsi version

    public function version(){

        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->versionRepository->all()->toArray();
        // var_dump($result);die;
        return view('admin.version.version',compact('result'));

    }  

    public function updateVersion(Request $request){

        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        
        // var_dump($request->all());
        // die;
        $token = Session::get('token');
        $name = Session::get('name');
       
        $admin = $this->adminRepository->findByField('name', $name )->toArray();
        if (!empty($admin)) {
            if ($token == $admin[0]["token"]){
                $input = $request->all();
                // print_r($input);die;
                $input = array("versionApp"=>$input['versionApp']);
                $id=1;
                $contact = $this->versionRepository->update($input, $id);
               
                return redirect('admin/version')->with('message','version has updated Succesfully');
            }
            else{
                return redirect('admin/version')->withErrors('error updating version. please try again!');
            }
            
        }
        else{
           Session::flush();
           return redirect('admin/login');    
        }


    }

    //fungsi tutorial
    public function listTutorial(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        $result = $this->tutorialRepository->orderBy('id','desc')->all()->toArray();
        // var_dump($result);die;
        return view('admin.tutorial.list',compact('result'));

    }

    public function addTutorial(){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        
        // var_dump($result);die;
        return view('admin.tutorial.add');

    }

    public function setTutorial(Request $request){
        $input = $request->all();

        $tutorial = $this->tutorialRepository->create($input);
        
        return redirect('admin/tutorial')->withErrors('Tutorial has created Succesfully');
        

    }

    public function editTutorial($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        

        $tutorial = $this->tutorialRepository->find($id);
        // var_dump($result);die;
        return view('admin.tutorial.edit',compact('tutorial'));

    }

    public function updateTutorial($id,Request $request){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        
        $input = $request->all();

        $tutorial = $this->tutorialRepository->update($input,$id);
        
        return redirect('admin/tutorial')->withErrors('Tutorial has updated Succesfully');

    }

    public function deleteTutorial($id){
        if(Session::get('token')==""||Session::get('name')==""){
            return redirect('admin/login');
            exit;
        }
        

        $tutorial = $this->tutorialRepository->delete($id);
        // var_dump($result);die;
        return redirect('admin/tutorial')->withErrors('Tutorial has deleted Succesfully');

    }


    //fungsi tambahan

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



    

}
