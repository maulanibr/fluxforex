<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\signalRepository;
use App\Models\signal;
use App\Repositories\contactRepository;
use App\Repositories\tutorialRepository;
use Carbon\Carbon;
use DB;
class PagesController extends Controller

{
    private $adminRepository;
    private $signalRepository;
    private $clientRepository;
    private $contactRepository;
    private $tutorialRepository;

    public function __construct(signalRepository $signalRepo,contactRepository $contactRepo,tutorialRepository $tutorialRepo)
    {
       
        $this->signalRepository = $signalRepo;
       
        $this->contactRepository = $contactRepo;
        $this->tutorialRepository= $tutorialRepo;
    }


    public function home(){
    	return view('pages.home');

    }

    public function performance($year = "",$month= ""){
        // echo $type;die;    
        // if($type==""){  
        //     $type='EUR - USD';
        // }else{
        //     $type = str_replace("%20"," ",$type);
        // }

        $yearList = DB::table('signals')
                ->select(DB::raw('distinct(YEAR(published)) as years'))
                ->orderBy('published','asc')
                ->get();   
        // echo "<pre>";print_r($yearList);echo "</pre>";die;    

        $currentYear = $this->getYear(Carbon::now());
        if($year){
            $selectYearandMonth = $year.'-'.$month;
        }else{
            $selectYearandMonth = $this->getYearandMonth(Carbon::now());
        }
        
        
        $data_stat = $this->signalRepository->orderBy('published','asc')->whereYear('published','=',$currentYear)->all()->toArray();
        
       
        $result_stat = $this->getDataPerformance($data_stat);
        
        
        
        $result = DB::select( DB::raw("SELECT * 
                                        FROM signals 
                                        WHERE DATE_FORMAT(published,'%Y-%m') = '$selectYearandMonth'
                                        and deleted_at is null
                                        and profit is not null
                                        order by published desc"
                                        ) );
        // $result = $this->signalRepository->whereNotnull('profit',$currentYearandMonth)->all();
        // echo "<pre>";print_r($result);echo "</pre>";die;
        if($year == "" && $month == ""){
            $year = $currentYear;
            $month = Carbon::now()->format('m');
        }
        
        // echo $month;die;

        return view('pages.performance',compact('result','result_stat','year','month','yearList'));
    }

    public function signal(){
        $result = $this->signalRepository->whereNotnull('profit')->all();
        // echo "<pre>";var_dump($result);echo "</pre>";die;
    	return view('pages.signal',compact('result'));
    }

    public function stat(){
        
       $currentYear = $this->getYear(Carbon::now());
        // var_dump($current);die;
       
        $result = $this->signalRepository->orderBy('published','asc')->whereYear('published','=',$currentYear)->all();
        // echo "<pre>";print_r($result);echo "</pre>";die;
        $eurusd = $this->getDataStat($result,'EUR - USD');
        $usdjpy = $this->getDataStat($result,'USD - JPY');
        $gbpusd = $this->getDataStat($result,'GBP - USD');
        $usdchf = $this->getDataStat($result,'USD - CHF');
        $eurgbp = $this->getDataStat($result,'EUR - GBP');
        $eurjpy = $this->getDataStat($result,'EUR - JPY');
        $eurchf = $this->getDataStat($result,'EUR - CHF');
        $audusd = $this->getDataStat($result,'AUD - USD');
        $usdcad = $this->getDataStat($result,'USD - CAD');
        $nzdusd = $this->getDataStat($result,'NZD - USD');
        $audjpy = $this->getDataStat($result,'AUD - JPY');
        $nzdjpy = $this->getDataStat($result,'NZD - JPY');
        $gbpjpy = $this->getDataStat($result,'GBP - JPY');
        $cadjpy = $this->getDataStat($result,'CAD - JPY');
        $euraud = $this->getDataStat($result,'EUR - AUD');
        $gbpaud = $this->getDataStat($result,'GBP - AUD');
        $gbpchf = $this->getDataStat($result,'GBP - CHF');
        $eurcad = $this->getDataStat($result,'EUR - CAD');
        $eurnzd = $this->getDataStat($result,'EUR - NZD');
        $gbpcad = $this->getDataStat($result,'GBP - CAD');

        $year = $currentYear;

        // echo "<pre>";print_r($eurusd);echo "</pre>";

        // echo "<pre>";print_r($gbpusd);echo "</pre>";die;
    	return view('pages.stat',compact('eurusd','usdjpy','gbpusd','usdchf','eurgbp','eurjpy','eurchf','audusd','usdcad','nzdusd','audjpy','nzdjpy','gbpjpy','cadjpy','euraud','gbpaud','gbpchf','eurcad','eurnzd','gbpcad','year'));
    }



    public function tutorial(){
    	return view('pages.tutorial');
    }

    public function contact(){
        $result = $this->contactRepository->all();
    	return view('pages.contact',compact('result'));
    }

    public function getDataPerformance($result){

        $dt = array();
        $resultYear = array("Jan"=>"","Feb"=>"","Mar"=>"","Apr"=>"","May"=>"","Jun"=>"","Jul"=>"","Aug"=>"","Sep"=>"","Okt"=>"","Nov"=>"","Des"=>"");
        foreach($result as $rs){
           
            
            $ymonth = $this->getYearMonth($rs->published);
            // echo $ymonth;die;

            if(empty($dt)){
               $temp = array('yearmonth'=>$ymonth,'profit'=>$rs->profit+0);     
               array_push($dt,$temp); 
            }else{
                // echo "<pre>";print_r($dt);echo "</pre>";
               
               
                if( $this->check_exist($dt,$ymonth)){
                   
                    $key = $this->getkey_array($dt,$ymonth);
                    // echo "<br> key = ".$key;
                    $profit = $dt[$key]['profit'];
                    $profit = ($profit+$rs->profit);
                    $dt[$key]['profit'] = $profit;
                }else{
                    $last = end($dt);
                    $temp = array('yearmonth'=>$ymonth,'profit'=>$rs->profit); 
                    array_push($dt,$temp);    
                }

            }
                
             // echo "<pre>";print_r($dt);echo "</pre>";    
            

            
              
            foreach($dt as $result){
                switch($this->getMonth($result['yearmonth'])){
                    case 'Jan' :
                        $resultYear['Jan'] = $result['profit'];
                        break;
                    case 'Feb' :
                        $resultYear['Feb'] = $result['profit'];
                        break;
                    case 'Mar' :
                        $resultYear['Mar'] = $result['profit'];
                        break;
                    case 'Apr' :
                        $resultYear['Apr'] = $result['profit'];
                        break;
                    case 'May' :
                        $resultYear['May'] = $result['profit'];
                        break;
                    case 'Jun' :
                        $resultYear['Jun'] = $result['profit'];
                        break;
                    case 'Jul' :
                        $resultYear['Jul'] = $result['profit'];
                        break;
                    case 'Aug' :
                        $resultYear['Aug'] = $result['profit'];
                        break;
                    case 'Sep' :
                        $resultYear['Sep'] = $result['profit'];
                        break;
                    case 'Okt' :
                        $resultYear['Okt'] = $result['profit'];
                        break;
                    case 'Nov' :
                        $resultYear['Nov'] = $result['profit'];
                        break;
                    case 'Des' :
                        $resultYear['Des'] = $result['profit'];
                        break;

                }
                

            }

                
            

        }
        // echo "<pre>";print_r($dt);echo "</pre>";die;
        return $resultYear;
                
                
            

    }

    public function getDataStat($result,$currency){
        $dt = array();
        foreach($result as $rs){
           
            if(strtolower($rs->currencyPair) == strtolower($currency)){
                $ymonth = $this->getYearMonth($rs->published);

                if(empty($dt)){
                   $temp = array('yearmonth'=>$ymonth,'profit'=>$rs->profit+$this->firstPoint($currency));     
                   array_push($dt,$temp); 
                }else{
                    // echo "<pre>";print_r($dt);echo "</pre>";
                    // echo $ymonth;
                    
                    // echo "<br>";
                   
                    if( $this->check_exist($dt,$ymonth)){
                        // var_dump($this->check_exist($dt,$ymonth));
                        // echo "<br>";
                        // var_dump($this->getkey_array($dt,$ymonth));
                        // echo "<br>";
                        $key = $this->getkey_array($dt,$ymonth);
                        // echo "<br> key = ".$key;
                        $profit = $dt[$key]['profit'];
                        $profit = ($profit+$rs->profit);
                        $dt[$key]['profit'] = $profit;
                    }else{
                        $last = end($dt);
                        $temp = array('yearmonth'=>$ymonth,'profit'=>$rs->profit+$last['profit']); 
                        array_push($dt,$temp);    
                    }

                }
                
                
            }

            $resultYear = array("Jan"=>"","Feb"=>"","Mar"=>"","Apr"=>"","May"=>"","Jun"=>"","Jul"=>"","Aug"=>"","Sep"=>"","Okt"=>"","Nov"=>"","Des"=>"");

            foreach($dt as $result){
                switch($this->getMonth($result['yearmonth'])){
                    case 'Jan' :
                        $resultYear['Jan'] = $result['profit'];
                        break;
                    case 'Feb' :
                        $resultYear['Feb'] = $result['profit'];
                        break;
                    case 'Mar' :
                        $resultYear['Mar'] = $result['profit'];
                        break;
                    case 'Apr' :
                        $resultYear['Apr'] = $result['profit'];
                        break;
                    case 'May' :
                        $resultYear['May'] = $result['profit'];
                        break;
                    case 'Jun' :
                        $resultYear['Jun'] = $result['profit'];
                        break;
                    case 'Jul' :
                        $resultYear['Jul'] = $result['profit'];
                        break;
                    case 'Aug' :
                        $resultYear['Aug'] = $result['profit'];
                        break;
                    case 'Sep' :
                        $resultYear['Sep'] = $result['profit'];
                        break;
                    case 'Okt' :
                        $resultYear['Okt'] = $result['profit'];
                        break;
                    case 'Nov' :
                        $resultYear['Nov'] = $result['profit'];
                        break;
                    case 'Des' :
                        $resultYear['Des'] = $result['profit'];
                        break;

                }
                

            }

                
            

        }
        // echo "<pre>";print_r($dt);echo "</pre>";die;
        return $resultYear;

    }

    public function firstPointTotal(){

        return 500;
    }

    public function firstPoint($currency){

        switch ($currency) {

            case 'EUR-USD' :
                $value = 500;
                break;
            case 'USD-JPY' :
                $value = 500;
                break;
            case 'GBP-USD' :
                $value = 500;
                break;
            case 'USD-CHF' :
                $value = 500;
                break;
            case 'EUR-GBP' :
                $value = 500;
                break;
            case 'EUR-JPY' :
                $value = 500;
                break;
            case 'EUR-CHF' :
                $value = 500;
                break;
            case 'AUD-USD' :
                $value = 500;
                break;
            case 'USD-CAD' :
                $value = 500;
                break;
            case 'NZD-USD' :
                $value = 500;
                break;
            case 'AUD-JPY' :
                $value = 500;
                break;
            case 'NZD-JPY' :
                $value = 500;
                break;
            case 'GBP-JPY' :
                $value = 500;
                break;
            case 'CAD-JPY' :
                $value = 500;
                break;
            case 'EUR-AUD' :
                $value = 500;
                break;
            case 'GBP-AUD' :
                $value = 500;
                break;
            case 'GBP-CHF' :
                $value = 500;
                break;
            case 'EUR-CAD' :
                $value = 500;
                break;
            case 'EUR-NZD' :
                $value = 500;
                break;
            case 'GBP-CAD' :
                $value = 500;
                break;
            default:
                $value = 0;
                break;

        }

        return $value;
            


    }

    public function getYearMonth($date){

        $ct = new Carbon($date);
        $yearmonth = $ct->format('Y M');
        return $yearmonth;
    }

    public function getMonth($date){

        $ct = new Carbon($date);
        $month = $ct->format('M');
        return $month;
    }

    public function getMonthtwoformat($date){

        $ct = new Carbon($date);
        $month = $ct->format('m');
        return $month;
    }

    public function getYear($date){
        $ct = new Carbon($date);
        $year = $ct->format('Y');
        return $year;
    }

    public function getYearandMonth($date){

        $ct = new Carbon($date);
        $month = $ct->format('Y-m');
        return $month;
    }



    public function getkey_array($array,$date){
        $keys = null;
        foreach($array as $key => $ar){
           if($ar['yearmonth']==$date){
                $keys = $key;
                break;

           }
        }
        return $key;
        

    }

    public function check_exist($array,$date){

        foreach($array as $ar){
            if($ar['yearmonth']==$date){
                return true;

            }
        }

        return false;
    }

}
