<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecontactAPIRequest;
use App\Http\Requests\API\UpdatecontactAPIRequest;
use App\Models\contact;
use App\Repositories\contactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class contactController
 * @package App\Http\Controllers\API
 */

class contactAPIController extends AppBaseController
{
    /** @var  contactRepository */
    private $contactRepository;

    public function __construct(contactRepository $contactRepo)
    {
        $this->contactRepository = $contactRepo;
    }

    // /**
    //  * @param Request $request
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/contacts",
    //  *      summary="Get a listing of the contacts.",
    //  *      tags={"contact"},
    //  *      description="Get all contacts",
    //  *      produces={"application/json"},
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="array",
    //  *                  @SWG\Items(ref="#/definitions/contact")
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    public function index(Request $request)
    {
        $this->contactRepository->pushCriteria(new RequestCriteria($request));
        $this->contactRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contacts = $this->contactRepository->all();

        return $this->sendResponse($contacts->toArray(), 'contacts retrieved successfully');
    }

    // /**
    //  * @param CreatecontactAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Post(
    //  *      path="/contacts",
    //  *      summary="Store a newly created contact in storage",
    //  *      tags={"contact"},
    //  *      description="Store contact",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="contact that should be stored",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/contact")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/contact"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function store(CreatecontactAPIRequest $request)
    // {
    //     $input = $request->all();

    //     $contacts = $this->contactRepository->create($input);

    //     return $this->sendResponse($contacts->toArray(), 'contact saved successfully');
    // }

    // *
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/contacts/{id}",
    //  *      summary="Display the specified contact",
    //  *      tags={"contact"},
    //  *      description="Get contact",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of contact",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/contact"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
     
    // public function show($id)
    // {
    //     /** @var contact $contact */
    //     $contact = $this->contactRepository->find($id);

    //     if (empty($contact)) {
    //         return Response::json(ResponseUtil::makeError('contact not found'), 400);
    //     }

    //     return $this->sendResponse($contact->toArray(), 'contact retrieved successfully');
    // }

    // /**
    //  * @param int $id
    //  * @param UpdatecontactAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Put(
    //  *      path="/contacts/{id}",
    //  *      summary="Update the specified contact in storage",
    //  *      tags={"contact"},
    //  *      description="Update contact",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of contact",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="contact that should be updated",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/contact")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/contact"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function update($id, UpdatecontactAPIRequest $request)
    // {
    //     $input = $request->all();

    //     /** @var contact $contact */
    //     $contact = $this->contactRepository->find($id);

    //     if (empty($contact)) {
    //         return Response::json(ResponseUtil::makeError('contact not found'), 400);
    //     }

    //     $contact = $this->contactRepository->update($input, $id);

    //     return $this->sendResponse($contact->toArray(), 'contact updated successfully');
    // }

    // /**
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Delete(
    //  *      path="/contacts/{id}",
    //  *      summary="Remove the specified contact from storage",
    //  *      tags={"contact"},
    //  *      description="Delete contact",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of contact",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="string"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function destroy($id)
    // {
    //     /** @var contact $contact */
    //     $contact = $this->contactRepository->find($id);

    //     if (empty($contact)) {
    //         return Response::json(ResponseUtil::makeError('contact not found'), 400);
    //     }

    //     $contact->delete();

    //     return $this->sendResponse($id, 'contact deleted successfully');
    // }
}
