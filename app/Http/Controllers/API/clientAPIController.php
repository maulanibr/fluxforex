<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateclientAPIRequest;
use App\Http\Requests\API\UpdateclientAPIRequest;
use App\Models\client;
use App\Repositories\clientRepository;
use App\Models\signal;
use App\Repositories\signalRepository;
use App\Models\tutorial;
use App\Repositories\tutorialRepository;
use Illuminate\Http\Request;
use Hash;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use JWTAuth;


/**
 * Class clientController
 * @package App\Http\Controllers\API
 */

class clientAPIController extends AppBaseController
{
    /** @var  clientRepository */
    private $clientRepository;
    private $signalRepository;
    private $tutorialRepository;
    public function __construct(clientRepository $clientRepo, signalRepository $signalRepo,tutorialRepository $tutorialRepo)
    {
        $this->clientRepository = $clientRepo;
        $this->signalRepository = $signalRepo;
        $this->tutorialRepository= $tutorialRepo;
    }

    // /**
    //  * @param Request $request
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/clients",
    //  *      summary="Get a listing of the clients.",
    //  *      tags={"client"},
    //  *      description="Get all clients",
    //  *      produces={"application/json"},
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="array",
    //  *                  @SWG\Items(ref="#/definitions/client")
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function index(Request $request)
    // {
    //     $this->clientRepository->pushCriteria(new RequestCriteria($request));
    //     $this->clientRepository->pushCriteria(new LimitOffsetCriteria($request));
    //     $clients = $this->clientRepository->all();

    //     return $this->sendResponse($clients->toArray(), 'clients retrieved successfully');
    // }

    // /**
    //  * @param CreateclientAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Post(
    //  *      path="/clients",
    //  *      summary="Store a newly created client in storage",
    //  *      tags={"client"},
    //  *      description="Store client",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="client that should be stored",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/client")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/client"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function store(CreateclientAPIRequest $request)
    // {
    //     $input = $request->all();
    //     $client = $this->clientRepository->findByField('email',$input['email']);
        
    //     if (empty($client->toArray())) {
    //         $input['password'] = Hash::make($input['password']);
    //         $clients = $this->clientRepository->create($input);

    //         return $this->sendResponse($clients->toArray(), 'client saved successfully');
    //     }
    //     else{
    //         return Response::json(ResponseUtil::makeError('name duplicate'), 400);
    //     }
        
    // }

    // /**
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/clients/{id}",
    //  *      summary="Display the specified client",
    //  *      tags={"client"},
    //  *      description="Get client",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of client",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/client"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function show($id)
    // {
    //     /** @var client $client */
    //     $client = $this->clientRepository->find($id);

    //     if (empty($client)) {
    //         return Response::json(ResponseUtil::makeError('client not found'), 400);
    //     }

    //     return $this->sendResponse($client->toArray(), 'client retrieved successfully');
    // }

    // /**
    //  * @param int $id
    //  * @param UpdateclientAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Put(
    //  *      path="/clients/{id}",
    //  *      summary="Update the specified client in storage",
    //  *      tags={"client"},
    //  *      description="Update client",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of client",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="client that should be updated",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/client")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/client"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function update($id, UpdateclientAPIRequest $request)
    // {
    //     $input = $request->all();

    //     /** @var client $client */
    //     $client = $this->clientRepository->find($id);

    //     if (empty($client)) {
    //         return Response::json(ResponseUtil::makeError('client not found'), 400);
    //     }

    //     $client = $this->clientRepository->update($input, $id);

    //     return $this->sendResponse($client->toArray(), 'client updated successfully');
    // }

    // /**
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Delete(
    //  *      path="/clients/{id}",
    //  *      summary="Remove the specified client from storage",
    //  *      tags={"client"},
    //  *      description="Delete client",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of client",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="string"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function destroy($id)
    // {
    //     /** @var client $client */
    //     $client = $this->clientRepository->find($id);

    //     if (empty($client)) {
    //         return Response::json(ResponseUtil::makeError('client not found'), 400);
    //     }

    //     $client->delete();

    //     return $this->sendResponse($id, 'client deleted successfully');
    // }



    // // public function login(CreateclientAPIRequest $request)
    // // {
    // //     $input = $request->only('email','password','gcmId');
    // //     $client = $this->clientRepository->findByField('email',$input['email'])->toArray();

    // //     if (empty($client)) {
    // //         return Response::json(ResponseUtil::makeError('user not found'), 400);
    // //     }
    // //     else{
    // //         if ($client[0]["password"] == $input['password']) {
    // //             $client = $this->clientRepository->update($input,$client[0]["id"]);
    // //             return $this->sendResponse($client, 'client login successfully');
    // //         }
    // //         else{
    // //             return Response::json(ResponseUtil::makeError('wrong password'), 400);       
    // //         }
            
            
    // //     }
        
    // // }


    public function subscribe(CreateclientAPIRequest $request)
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                 return Response::json(ResponseUtil::makeError('user not found'), 400);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return Response::json(ResponseUtil::makeError('token expired'), 400);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return Response::json(ResponseUtil::makeError('token invalid'), 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return Response::json(ResponseUtil::makeError('token absent'), 400);
        }

        // the token is valid and we have found the user via the sub claim
        $input = $request->only('email','month');
        $client = $this->clientRepository->findByField('email',$input['email'])->toArray();
        if (empty($client)) {
            return Response::json(ResponseUtil::makeError('user not found'), 400);
        }
        else{
            $month = intval($input['month']);
            $input['endSubscribe'] = date('Y-m-d');
            $time = strtotime($client[0]['endSubscribe']);
            $input['endSubscribe'] = date("Y-m-d", strtotime("+".$month." month", $time));
            $client = $this->clientRepository->update($input,$client[0]["id"]);
            return $this->sendResponse($client, 'client subscribe successfully');
        }   
            
    }

    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $input = $request->only('email','gcm_id');
        $client = $this->clientRepository->findByField('email',$input['email'])->toArray();
        $client[0]["gcmId"] = $input['gcm_id'];
        $client = $this->clientRepository->update($client[0], $client[0]["id"]);
        // all good so return the token
        return response()->json(compact('token'));
    }
    public function updateGcmid(Request $request){
        $input = $request->only('gcmId');
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $user->__set("gcmId",$input["gcmId"]);
        return $this->sendResponse([], 'gcmId update successfully');
    }
    public function register(Request $request)
    {
        $input = $request->all();
        $client = $this->clientRepository->findByField('email',$input['email']);
        
        if (empty($client->toArray())) {
            $input['password'] = Hash::make($input['password']);
            $clients = $this->clientRepository->create($input);
            return $this->sendResponse([], 'client saved successfully');
        }
        else{
            return Response::json(ResponseUtil::makeError('name duplicate'), 400);
        }
        
    }

    public function changePassword(Request $request)
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                 return Response::json(ResponseUtil::makeError('user not found'), 400);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return Response::json(ResponseUtil::makeError('token expired'), 400);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return Response::json(ResponseUtil::makeError('token invalid'), 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return Response::json(ResponseUtil::makeError('token absent'), 400);
        }

        $input = $request->only('email','newPassword','oldPassword');
        /** @var client $client */
        $client = $this->clientRepository->findByField('email',$input['email'])->toArray();

        if (empty($client)) {
            return Response::json(ResponseUtil::makeError('client not found'), 400);
        }
        else{
            if (Hash::check($input['oldPassword'], $client[0]["password"]))
            {
                $input['password'] = Hash::make($input['newPassword']);
                $client = $this->clientRepository->update($input, $client[0]["id"]);
                return $this->sendResponse($client->toArray(), 'client updated successfully');
            }
            
            else {
                return Response::json(ResponseUtil::makeError('password not match'), 400);
            }
        }
        
    }
    public function getSignal(Request $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                 return Response::json(ResponseUtil::makeError('user not found'), 400);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return Response::json(ResponseUtil::makeError('token expired'), 400);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return Response::json(ResponseUtil::makeError('token invalid'), 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return Response::json(ResponseUtil::makeError('token absent'), 400);
        }

        $input = $request->only('email');
        $client = $this->clientRepository->findByField('email',$input['email'])->toArray();
        if (empty($client)) {
            return Response::json(ResponseUtil::makeError('client not found'), 400);
        }
        else {
            if($client[0]['endSubscribe'] > date("Y-m-d") ){
                $signal=$this->signalRepository->all();
                return $this->sendResponse($signal->toArray(), 'signal get successfully');
            }
            else {
                $signal=$this->signalRepository->findWhere([ ['published', '<', date("Y-m-d")] ]);
                return $this->sendResponse($signal->toArray(), 'old signal get successfully');
            }
        }
    }
    public function getsignalbyId(Request $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                 return Response::json(ResponseUtil::makeError('user not found'), 400);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return Response::json(ResponseUtil::makeError('token expired'), 400);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return Response::json(ResponseUtil::makeError('token invalid'), 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return Response::json(ResponseUtil::makeError('token absent'), 400);
        }

        $input = $request->only('id');
        $signal=$this->signalRepository->findWhere(["id" => $input['id']]);
        if (empty($signal->toArray())) {
            return Response::json(ResponseUtil::makeError('signal not found'), 400);
        }
        else {
            return $this->sendResponse($signal->toArray(), 'signal get successfully');    
        }
        
    }
    public function getTutorial(Request $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                 return Response::json(ResponseUtil::makeError('user not found'), 400);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return Response::json(ResponseUtil::makeError('token expired'), 400);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return Response::json(ResponseUtil::makeError('token invalid'), 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return Response::json(ResponseUtil::makeError('token absent'), 400);
        }

        $tutorial=$this->tutorialRepository->all();
        if (empty($tutorial->toArray())) {
            return Response::json(ResponseUtil::makeError('tutorial not found'), 400);
        }
        else {
            return $this->sendResponse($tutorial->toArray(), 'tutorial get successfully');    
        }
        
    }
    public function getTutorialbyId(Request $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                 return Response::json(ResponseUtil::makeError('user not found'), 400);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return Response::json(ResponseUtil::makeError('token expired'), 400);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return Response::json(ResponseUtil::makeError('token invalid'), 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return Response::json(ResponseUtil::makeError('token absent'), 400);
        }

        $input = $request->only('id');
        $tutorial=$this->tutorialRepository->findWhere(["id" => $input['id']]);
        if (empty($tutorial->toArray())) {
            return Response::json(ResponseUtil::makeError('tutorial not found'), 400);
        }
        else {
            return $this->sendResponse($tutorial->toArray(), 'tutorial get successfully');    
        }
        
    }
}
