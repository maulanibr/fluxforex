<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatenoteAPIRequest;
use App\Http\Requests\API\UpdatenoteAPIRequest;
use App\Models\note;
use App\Repositories\noteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class noteController
 * @package App\Http\Controllers\API
 */

class noteAPIController extends AppBaseController
{
    /** @var  noteRepository */
    private $noteRepository;

    public function __construct(noteRepository $noteRepo)
    {
        $this->noteRepository = $noteRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/notes",
     *      summary="Get a listing of the notes.",
     *      tags={"note"},
     *      description="Get all notes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/note")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->noteRepository->pushCriteria(new RequestCriteria($request));
        $this->noteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $notes = $this->noteRepository->all();

        return $this->sendResponse($notes->toArray(), 'notes retrieved successfully');
    }

    /**
     * @param CreatenoteAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/notes",
     *      summary="Store a newly created note in storage",
     *      tags={"note"},
     *      description="Store note",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="note that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/note")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/note"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatenoteAPIRequest $request)
    {
        $input = $request->all();

        $notes = $this->noteRepository->create($input);

        return $this->sendResponse($notes->toArray(), 'note saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/notes/{id}",
     *      summary="Display the specified note",
     *      tags={"note"},
     *      description="Get note",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of note",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/note"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var note $note */
        $note = $this->noteRepository->find($id);

        if (empty($note)) {
            return Response::json(ResponseUtil::makeError('note not found'), 400);
        }

        return $this->sendResponse($note->toArray(), 'note retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatenoteAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/notes/{id}",
     *      summary="Update the specified note in storage",
     *      tags={"note"},
     *      description="Update note",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of note",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="note that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/note")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/note"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatenoteAPIRequest $request)
    {
        $input = $request->all();

        /** @var note $note */
        $note = $this->noteRepository->find($id);

        if (empty($note)) {
            return Response::json(ResponseUtil::makeError('note not found'), 400);
        }

        $note = $this->noteRepository->update($input, $id);

        return $this->sendResponse($note->toArray(), 'note updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/notes/{id}",
     *      summary="Remove the specified note from storage",
     *      tags={"note"},
     *      description="Delete note",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of note",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var note $note */
        $note = $this->noteRepository->find($id);

        if (empty($note)) {
            return Response::json(ResponseUtil::makeError('note not found'), 400);
        }

        $note->delete();

        return $this->sendResponse($id, 'note deleted successfully');
    }
}
