<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatetutorialAPIRequest;
use App\Http\Requests\API\UpdatetutorialAPIRequest;
use App\Models\tutorial;
use App\Repositories\tutorialRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class tutorialController
 * @package App\Http\Controllers\API
 */

class tutorialAPIController extends AppBaseController
{
    /** @var  tutorialRepository */
    private $tutorialRepository;

    public function __construct(tutorialRepository $tutorialRepo)
    {
        $this->tutorialRepository = $tutorialRepo;
    }

    // /**
    //  * @param Request $request
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/tutorials",
    //  *      summary="Get a listing of the tutorials.",
    //  *      tags={"tutorial"},
    //  *      description="Get all tutorials",
    //  *      produces={"application/json"},
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="array",
    //  *                  @SWG\Items(ref="#/definitions/tutorial")
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function index(Request $request)
    // {
    //     $this->tutorialRepository->pushCriteria(new RequestCriteria($request));
    //     $this->tutorialRepository->pushCriteria(new LimitOffsetCriteria($request));
    //     $tutorials = $this->tutorialRepository->all();

    //     return $this->sendResponse($tutorials->toArray(), 'tutorials retrieved successfully');
    // }

    // /**
    //  * @param CreatetutorialAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Post(
    //  *      path="/tutorials",
    //  *      summary="Store a newly created tutorial in storage",
    //  *      tags={"tutorial"},
    //  *      description="Store tutorial",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="tutorial that should be stored",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/tutorial")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/tutorial"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function store(CreatetutorialAPIRequest $request)
    // {
    //     $input = $request->all();

    //     $tutorials = $this->tutorialRepository->create($input);

    //     return $this->sendResponse($tutorials->toArray(), 'tutorial saved successfully');
    // }

    // *
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/tutorials/{id}",
    //  *      summary="Display the specified tutorial",
    //  *      tags={"tutorial"},
    //  *      description="Get tutorial",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of tutorial",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/tutorial"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
     
    // public function show($id)
    // {
    //     /** @var tutorial $tutorial */
    //     $tutorial = $this->tutorialRepository->find($id);

    //     if (empty($tutorial)) {
    //         return Response::json(ResponseUtil::makeError('tutorial not found'), 400);
    //     }

    //     return $this->sendResponse($tutorial->toArray(), 'tutorial retrieved successfully');
    // }

    // /**
    //  * @param int $id
    //  * @param UpdatetutorialAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Put(
    //  *      path="/tutorials/{id}",
    //  *      summary="Update the specified tutorial in storage",
    //  *      tags={"tutorial"},
    //  *      description="Update tutorial",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of tutorial",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="tutorial that should be updated",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/tutorial")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/tutorial"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function update($id, UpdatetutorialAPIRequest $request)
    // {
    //     $input = $request->all();

    //     /** @var tutorial $tutorial */
    //     $tutorial = $this->tutorialRepository->find($id);

    //     if (empty($tutorial)) {
    //         return Response::json(ResponseUtil::makeError('tutorial not found'), 400);
    //     }

    //     $tutorial = $this->tutorialRepository->update($input, $id);

    //     return $this->sendResponse($tutorial->toArray(), 'tutorial updated successfully');
    // }

    // /**
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Delete(
    //  *      path="/tutorials/{id}",
    //  *      summary="Remove the specified tutorial from storage",
    //  *      tags={"tutorial"},
    //  *      description="Delete tutorial",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of tutorial",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="string"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function destroy($id)
    // {
    //     /** @var tutorial $tutorial */
    //     $tutorial = $this->tutorialRepository->find($id);

    //     if (empty($tutorial)) {
    //         return Response::json(ResponseUtil::makeError('tutorial not found'), 400);
    //     }

    //     $tutorial->delete();

    //     return $this->sendResponse($id, 'tutorial deleted successfully');
    // }
    public function getTutorial(){
        $tutorials=$this->tutorialRepository->all();
        return $this->sendResponse($tutorials->toArray(), 'tutorial get successfully');
    }
}
