<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateadminAPIRequest;
use App\Http\Requests\API\UpdateadminAPIRequest;
use App\Models\admin;
use App\Repositories\adminRepository;
use App\Models\signal;
use App\Repositories\signalRepository;
use App\Models\client;
use App\Repositories\clientRepository;
use App\Models\contact;
use App\Repositories\contactRepository;
use App\Repositories\tutorialRepository;
use App\Repositories\noteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Hash;
use App\Models\GCM;
/**
 * Class adminController
 * @package App\Http\Controllers\API
 */

class adminAPIController extends AppBaseController
{
    /** @var  adminRepository */
    private $adminRepository;
    private $signalRepository;
    private $clientRepository;
    private $contactRepository;
    private $tutorialRepository;
    private $noteRepository;
    public function __construct(adminRepository $adminRepo, signalRepository $signalRepo,clientRepository $clientRepo,contactRepository $contactRepo,tutorialRepository $tutorialRepo,noteRepository $noteRepo)
    {
        $this->adminRepository = $adminRepo;
        $this->signalRepository = $signalRepo;
        $this->clientRepository = $clientRepo;
        $this->contactRepository = $contactRepo;
        $this->tutorialRepository= $tutorialRepo;
        $this->noteRepository = $noteRepo;
    }

    // /**
    //  * @param Request $request
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/admins",
    //  *      summary="Get a listing of the admins.",
    //  *      tags={"admin"},
    //  *      description="Get all admins",
    //  *      produces={"application/json"},
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="array",
    //  *                  @SWG\Items(ref="#/definitions/admin")
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function index(Request $request)
    // {
    //     $this->adminRepository->pushCriteria(new RequestCriteria($request));
    //     $this->adminRepository->pushCriteria(new LimitOffsetCriteria($request));
    //     $admins = $this->adminRepository->all();

    //     return $this->sendResponse($admins->toArray(), 'admins retrieved successfully');
    // }

    // /**
    //  * @param CreateadminAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Post(
    //  *      path="/admins",
    //  *      summary="Store a newly created admin in storage",
    //  *      tags={"admin"},
    //  *      description="Store admin",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="admin that should be stored",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/admin")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/admin"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function store(CreateadminAPIRequest $request)
    // {
    //     $input = $request->all();
    //     $input['password'] = Hash::make($input['password']);

    //     $admins = $this->adminRepository->create($input);

    //     return $this->sendResponse($admins->toArray(), 'admin saved successfully');
    // }

    // *
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/admins/{id}",
    //  *      summary="Display the specified admin",
    //  *      tags={"admin"},
    //  *      description="Get admin",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of admin",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/admin"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
     
    // public function show($id)
    // {
    //     /** @var admin $admin */
    //     $admin = $this->adminRepository->find($id);

    //     if (empty($admin)) {
    //         return Response::json(ResponseUtil::makeError('admin not found'), 400);
    //     }

    //     return $this->sendResponse($admin->toArray(), 'admin retrieved successfully');
    // }

    // /**
    //  * @param int $id
    //  * @param UpdateadminAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Put(
    //  *      path="/admins/{id}",
    //  *      summary="Update the specified admin in storage",
    //  *      tags={"admin"},
    //  *      description="Update admin",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of admin",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="admin that should be updated",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/admin")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/admin"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function update($id, UpdateadminAPIRequest $request)
    // {
    //     $input = $request->all();

    //     /** @var admin $admin */
    //     $admin = $this->adminRepository->find($id);

    //     if (empty($admin)) {
    //         return Response::json(ResponseUtil::makeError('admin not found'), 400);
    //     }

    //     $admin = $this->adminRepository->update($input, $id);

    //     return $this->sendResponse($admin->toArray(), 'admin updated successfully');
    // }

    // /**
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Delete(
    //  *      path="/admins/{id}",
    //  *      summary="Remove the specified admin from storage",
    //  *      tags={"admin"},
    //  *      description="Delete admin",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of admin",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="string"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function destroy($id)
    // {
    //     /** @var admin $admin */
    //     $admin = $this->adminRepository->find($id);

    //     if (empty($admin)) {
    //         return Response::json(ResponseUtil::makeError('admin not found'), 400);
    //     }

    //     $admin->delete();

    //     return $this->sendResponse($id, 'admin deleted successfully');
    // }

    public function login(CreateadminAPIRequest $request){
        $input = $request->only('name','password');
        $input['token'] = $this->generateRandomString();
        /** @var admin $admin */
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if (Hash::check($input['password'], $admin[0]["password"])){
                $input['password'] = Hash::make($input['password']);
                $admin = $this->adminRepository->update($input,$admin[0]['id']);
                return $this->sendResponse($admin, 'admin login successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('password not match'), 400);    
            }
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    
    public function setAdmin(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $input = $request->only('data')['data'];
                $input['password'] = Hash::make($input['password']);
                $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
                if (empty($admin)) {
                    $admins = $this->adminRepository->create($input);
                    return $this->sendResponse($admins->toArray(), 'admin saved successfully');
                }
                else{
                    return Response::json(ResponseUtil::makeError('admin exist'), 400);    
                }
            }
            else{
                return Response::json(ResponseUtil::makeError('unauthorized'), 400);
            }
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    public function getAdmibyId(Request $request){
        $input = $request->only('name','token',"id");
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $admin = $this->adminRepository->findWhere(["id" => $input['id']]);
                if (empty($admin->toArray())) {
                    return Response::json(ResponseUtil::makeError('admin not found'), 400);
                }
                else{
                    return $this->sendResponse($admin->toArray(), 'admin get successfully');    
                }
                
            }
            else{
                return Response::json(ResponseUtil::makeError('unauthorized'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function deleteAdmin(Request $request){
        $input = $request->only('name','token',"id");
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $admin = $this->adminRepository->find($input['id']);
                $admin->delete();
                return $this->sendResponse($id, 'admin deleted successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('unauthorized'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function setSignal(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"]){
                $input = $request->only('data')['data'];
                
                if($input){
                    foreach ($input as $key) {
                        if($key['type']=="signal"){
                            $signal = $this->signalRepository->create($key);
                        }
                    
                    }
                    $this->sendGCM($input);
                    return $this->sendResponse([], 'signal created successfully');
                    
                }else{
                    return Response::json(ResponseUtil::makeError('field empty'), 400);
                }
                
               
                //$signal = $this->signalRepository->create($input);
                //return $this->sendResponse($signal->toArray(), 'signal created successfully');
               
                
                //$clients=$this->clientRepository->findWhere([ ['endSubscribe', '>=', date("Y-m-d")] ]); ini buat get subscribe
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    private function sendGCM($input){
        $clients=$this->clientRepository->findWhere([ ['endSubscribe', '>=', date("Y-m-d")] ]); //ini buat get subscribe
        $reg_id = array();
        foreach ($clients as $key) {
            array_push($reg_id, $key["gcmId"]);
        }
        $gcm = new GCM();
        foreach ($input as $key) {
            $data =  array('notification' => array('body' => $key['currencyPair'],
                                                'title' => 'fluxforex' ),
                            'data' => $key );
            //var_dump($data);
            //var_dump($gcm->send_notification($reg_id, $data));
            $message =  array('message' => $data );
            $gcm->send_notification($reg_id, $message);
        }
        die;
    }
    public function getSignal(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"]){
                // $input = $request->only('data')['data'];
                // $input['password'] = Hash::make($input['password']);
                $signal = $this->signalRepository->all();
                return $this->sendResponse($signal->toArray(), 'signal get successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    public function getSignalbyId(Request $request){
        $input = $request->only('name','token','id');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"]){
                // $input = $request->only('data')['data'];
                // $input['password'] = Hash::make($input['password']);
                $signal = $this->signalRepository->findWhere(["id" => $input['id']]);
                if(empty($signal->toArray())) {
                    return Response::json(ResponseUtil::makeError('signal not found'), 400);
                }
                else{
                    return $this->sendResponse($signal->toArray(), 'signal get successfully');    
                }
                
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    public function deleteSignal(Request $request){
        $input = $request->only('name','token',"id");
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $signal = $this->signalRepository->find($input['id']);
                $signal->delete();
                return $this->sendResponse($signal, 'signal deleted successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    public function updateSignal(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"]){
                $input = $request->only('data')['data'];
                $id = $request->only('id')['id'];
                $signal = $this->signalRepository->update($input, $id);
                return $this->sendResponse($id, 'signal update successfully');
                //$clients=$this->clientRepository->findWhere([ ['endSubscribe', '>=', date("Y-m-d")] ]); ini buat get subscribe
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    public function getClient(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $client = $this->clientRepository->all();
                return $this->sendResponse($client->toArray(), 'client get successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function getClientSubscribe(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"]){
                $clients=$this->clientRepository->findWhere([ ['endSubscribe', '>=', date("Y-m-d")] ]);
                return $this->sendResponse($clients->toArray(), 'client get successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }
    }
    public function getClientById(Request $request){
        $input = $request->only('name','token','id');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $client = $this->clientRepository->findWhere(["id" => $input['id']]);
                if (empty($client->toArray())){
                    return Response::json(ResponseUtil::makeError('client not found'), 400);    
                }
                else{
                    return $this->sendResponse($client->toArray(), 'client get successfully');    
                }
                
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function deleteClient(Request $request){
        $input = $request->only('name','token','id');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $client = $this->clientRepository->find($input['id']);
                $client->delete();
                return $this->sendResponse($client->toArray(), 'client deleted successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function setContact(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $input = $request->only('data')['data'];
                $contacts = $this->contactRepository->create($input);
                return $this->sendResponse($contacts->toArray(), 'contact created successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function deleteContact(Request $request){
        $input = $request->only('name','token','id');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $contacts = $this->contactRepository->find($input['id']);
                $contacts->delete();
                return $this->sendResponse($contacts->toArray(), 'contact created successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function updateContact(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $input = $request->only('data')['data'];
                $id = $request->only('id')['id'];
                $contact = $this->contactRepository->update($input, $id);
                return $this->sendResponse($contact->toArray(), 'contact updated successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function setTutorial(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $input = $request->only('data')['data'];
                $tutorial = $this->tutorialRepository->create($input);
                return $this->sendResponse($tutorial->toArray(), 'tutorial created successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function updateTutorial(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $input = $request->only('data')['data'];
                $id = $request->only('id')['id'];
                $tutorial = $this->tutorialRepository->update($input,$id);
                return $this->sendResponse($tutorial->toArray(), 'tutorial created successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function getTutorial(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $tutorial = $this->tutorialRepository->all();
                return $this->sendResponse($tutorial->toArray(), 'tutorial get successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function getTutorialbyId(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $id = $request->only('id')['id'];
                $tutorial = $this->tutorialRepository->findWhere(["id" => $id]);
                if (empty($tutorial->toArray())) {
                    return Response::json(ResponseUtil::makeError('tutorial not found'), 400);
                }
                else{
                    return $this->sendResponse($tutorial->toArray(), 'tutorial get successfully');    
                }
                
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function deleteTutorial(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $id = $request->only('id')['id'];
                $tutorial = $this->tutorialRepository->find($id);
                $tutorial->delete();
                return $this->sendResponse($tutorial->toArray(), 'tutorial get successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    public function setNote(Request $request){
        $input = $request->only('name','token');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] ){
                $input = $request->only('data')['data'];
                $note = $this->noteRepository->create($input);
                return $this->sendResponse($note->toArray(), 'note set successfully');
                //$clients=$this->clientRepository->findWhere([ ['endSubscribe', '>=', date("Y-m-d")] ]); ini buat get subscribe
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
    }
    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
