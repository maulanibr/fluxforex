<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVersionAPIRequest;
use App\Http\Requests\API\UpdateVersionAPIRequest;
use App\Models\Version;
use App\Repositories\VersionRepository;
use App\Models\admin;
use App\Repositories\adminRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VersionController
 * @package App\Http\Controllers\API
 */

class versionAPIController extends AppBaseController
{
    /** @var  VersionRepository */
    private $versionRepository;
    private $adminRepository;
    public function __construct(VersionRepository $versionRepo, adminRepository $adminRepo)
    {
        $this->versionRepository = $versionRepo;
        $this->adminRepository = $adminRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/versions",
     *      summary="Get a listing of the Versions.",
     *      tags={"Version"},
     *      description="Get all Versions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Version")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->versionRepository->pushCriteria(new RequestCriteria($request));
        $this->versionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $versions = $this->versionRepository->all();

        return $this->sendResponse($versions->toArray(), 'Versions retrieved successfully');
    }

    /**
     * @param CreateVersionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/versions",
     *      summary="Store a newly created Version in storage",
     *      tags={"Version"},
     *      description="Store Version",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Version that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Version")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Version"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateVersionAPIRequest $request)
    {
        $input = $request->all();

        $versions = $this->versionRepository->create($input);

        return $this->sendResponse($versions->toArray(), 'Version saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/versions/{id}",
     *      summary="Display the specified Version",
     *      tags={"Version"},
     *      description="Get Version",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Version",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Version"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Version $version */
        $version = $this->versionRepository->find($id);

        if (empty($version)) {
            return Response::json(ResponseUtil::makeError('Version not found'), 400);
        }

        return $this->sendResponse($version->toArray(), 'Version retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateVersionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/versions/{id}",
     *      summary="Update the specified Version in storage",
     *      tags={"Version"},
     *      description="Update Version",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Version",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Version that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Version")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Version"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getVersion(){
        $version = $this->versionRepository->all();
        return $this->sendResponse($version[count($version->toArray())-1]->versionApp, 'Get version successfully');
    }
    public function setVersion(Request $request){
        $input = $request->only('name','token','id');
        $admin = $this->adminRepository->findByField('name',$input['name'])->toArray();
        if (!empty($admin)) {
            if ($input['token'] == $admin[0]["token"] && $admin[0]['privilege'] == "super"){
                $req = $request->only('versionApp');
                $versions = $this->versionRepository->create($req);
                return $this->sendResponse($versions->toArray(), 'Version saved successfully');
            }
            else{
                return Response::json(ResponseUtil::makeError('error'), 400);
            }
            
        }
        else{
            return Response::json(ResponseUtil::makeError('admin not found'), 400);    
        }   
        
    }
    public function update($id, UpdateVersionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Version $version */
        $version = $this->versionRepository->find($id);

        if (empty($version)) {
            return Response::json(ResponseUtil::makeError('Version not found'), 400);
        }

        $version = $this->versionRepository->update($input, $id);

        return $this->sendResponse($version->toArray(), 'Version updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/versions/{id}",
     *      summary="Remove the specified Version from storage",
     *      tags={"Version"},
     *      description="Delete Version",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Version",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Version $version */
        $version = $this->versionRepository->find($id);

        if (empty($version)) {
            return Response::json(ResponseUtil::makeError('Version not found'), 400);
        }

        $version->delete();

        return $this->sendResponse($id, 'Version deleted successfully');
    }
}
