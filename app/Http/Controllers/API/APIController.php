<?php 

namespace App\Http\Controllers\API;


use App\Models\admin;
use App\Repositories\adminRepository;
use App\Repositories\signalRepository;
use App\Repositories\clientRepository;
use App\Repositories\tutorialRepository;
use App\Repositories\contactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;/**
 * Class APIController
 * @package App\Http\Controllers\API
 */

class APIController extends AppBaseController
{
    /** @var repository */
    private $adminRepository;
    private $clientRepository;
    private $tutorialRepository;
    private $signalRepository;
	private $contactRepository;

    public function __construct(adminRepository $adminRepo,clientRepository $clientRepo,contactRepository $contactRepo,signalRepository $signalRepo,tutorialRepository $tutorialRepo)
    {
        $this->adminRepository = $adminRepo;
        $this->clientRepository = $clientRepo;
        $this->tutorialRepository = $tutorialRepo;
        $this->contactRepository = $contactRepo;
        $this->signalRepository = $signalRepo;
    }

    public function index(Request $request)
    {
        $this->adminRepository->pushCriteria(new RequestCriteria($request));
        $this->adminRepository->pushCriteria(new LimitOffsetCriteria($request));
        $admins = $this->adminRepository->all();

        return $this->sendResponse($admins->toArray(), 'admins retrieved successfully');
    }
}