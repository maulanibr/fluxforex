<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatesignalAPIRequest;
use App\Http\Requests\API\UpdatesignalAPIRequest;
use App\Models\signal;
use App\Repositories\signalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class signalController
 * @package App\Http\Controllers\API
 */

class signalAPIController extends AppBaseController
{
    /** @var  signalRepository */
    private $signalRepository;

    public function __construct(signalRepository $signalRepo)
    {
        $this->signalRepository = $signalRepo;
    }

    // /**
    //  * @param Request $request
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/signals",
    //  *      summary="Get a listing of the signals.",
    //  *      tags={"signal"},
    //  *      description="Get all signals",
    //  *      produces={"application/json"},
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="array",
    //  *                  @SWG\Items(ref="#/definitions/signal")
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function index(Request $request)
    // {
    //     $this->signalRepository->pushCriteria(new RequestCriteria($request));
    //     $this->signalRepository->pushCriteria(new LimitOffsetCriteria($request));
    //     $signals = $this->signalRepository->all();

    //     return $this->sendResponse($signals->toArray(), 'signals retrieved successfully');
    // }

    // /**
    //  * @param CreatesignalAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Post(
    //  *      path="/signals",
    //  *      summary="Store a newly created signal in storage",
    //  *      tags={"signal"},
    //  *      description="Store signal",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="signal that should be stored",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/signal")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/signal"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function store(CreatesignalAPIRequest $request)
    // {
    //     $input = $request->all();

    //     $signals = $this->signalRepository->create($input);

    //     return $this->sendResponse($signals->toArray(), 'signal saved successfully');
    // }

    // *
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Get(
    //  *      path="/signals/{id}",
    //  *      summary="Display the specified signal",
    //  *      tags={"signal"},
    //  *      description="Get signal",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of signal",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/signal"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
     
    // public function show($id)
    // {
    //     /** @var signal $signal */
    //     $signal = $this->signalRepository->find($id);

    //     if (empty($signal)) {
    //         return Response::json(ResponseUtil::makeError('signal not found'), 400);
    //     }

    //     return $this->sendResponse($signal->toArray(), 'signal retrieved successfully');
    // }

    // /**
    //  * @param int $id
    //  * @param UpdatesignalAPIRequest $request
    //  * @return Response
    //  *
    //  * @SWG\Put(
    //  *      path="/signals/{id}",
    //  *      summary="Update the specified signal in storage",
    //  *      tags={"signal"},
    //  *      description="Update signal",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of signal",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Parameter(
    //  *          name="body",
    //  *          in="body",
    //  *          description="signal that should be updated",
    //  *          required=false,
    //  *          @SWG\Schema(ref="#/definitions/signal")
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  ref="#/definitions/signal"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function update($id, UpdatesignalAPIRequest $request)
    // {
    //     $input = $request->all();

    //     /** @var signal $signal */
    //     $signal = $this->signalRepository->find($id);

    //     if (empty($signal)) {
    //         return Response::json(ResponseUtil::makeError('signal not found'), 400);
    //     }

    //     $signal = $this->signalRepository->update($input, $id);

    //     return $this->sendResponse($signal->toArray(), 'signal updated successfully');
    // }

    // /**
    //  * @param int $id
    //  * @return Response
    //  *
    //  * @SWG\Delete(
    //  *      path="/signals/{id}",
    //  *      summary="Remove the specified signal from storage",
    //  *      tags={"signal"},
    //  *      description="Delete signal",
    //  *      produces={"application/json"},
    //  *      @SWG\Parameter(
    //  *          name="id",
    //  *          description="id of signal",
    //  *          type="integer",
    //  *          required=true,
    //  *          in="path"
    //  *      ),
    //  *      @SWG\Response(
    //  *          response=200,
    //  *          description="successful operation",
    //  *          @SWG\Schema(
    //  *              type="object",
    //  *              @SWG\Property(
    //  *                  property="success",
    //  *                  type="boolean"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="data",
    //  *                  type="string"
    //  *              ),
    //  *              @SWG\Property(
    //  *                  property="message",
    //  *                  type="string"
    //  *              )
    //  *          )
    //  *      )
    //  * )
    //  */
    // public function destroy($id)
    // {
    //     /** @var signal $signal */
    //     $signal = $this->signalRepository->find($id);

    //     if (empty($signal)) {
    //         return Response::json(ResponseUtil::makeError('signal not found'), 400);
    //     }

    //     $signal->delete();

    //     return $this->sendResponse($id, 'signal deleted successfully');
    // }
    public function getsignal(){
        $signal=$this->signalRepository->findWhere([ ['published', '<', date("Y-m-d")] ]);
        return $this->sendResponse($signal->toArray(), 'old signal get successfully');
    }
}
