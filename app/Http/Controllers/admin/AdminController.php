<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\signalRepository;

use App\Repositories\contactRepository;
use App\Repositories\tutorialRepository;
use Carbon\Carbon;

class AdminController extends Controller

{
    private $adminRepository;
    private $signalRepository;
    private $clientRepository;
    private $contactRepository;
    private $tutorialRepository;

    public function __construct(signalRepository $signalRepo,contactRepository $contactRepo,tutorialRepository $tutorialRepo)
    {
       
        $this->signalRepository = $signalRepo;
       
        $this->contactRepository = $contactRepo;
        $this->tutorialRepository= $tutorialRepo;
    }


    public function dashboard(){
        
    	return view('pages.dashboard');

    }

    

}
