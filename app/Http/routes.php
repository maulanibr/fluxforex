<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', [
	'as' => 'home',
	'uses' => 'PagesController@home'
]); 

Route::get('/signal', [
	'as' => 'signal',
	'uses' => 'PagesController@signal'
]); 

Route::get('/stat', [
	'as' => 'stat',
	'uses' => 'PagesController@stat'
]); 

// Route::get('/performance', [
// 	'as' => 'performance',
// 	'uses' => 'PagesController@performance'
// ]); 

Route::get('/performance/{year?}/{month?}', [
	'as' => 'performance',
	'uses' => 'PagesController@performance'
]); 

// Route::get('/tutorial', [
// 	'as' => 'tutorial',
// 	'uses' => 'PagesController@tutorial'
// ]); 

Route::get('/contact', [
	'as' => 'contact',
	'uses' => 'PagesController@contact'
]); 

//admin cms



Route::group(['prefix' => '/admin'],function(){
	Route::get('/', [
		'as' => 'dashboard',
		'uses' => 'AdminController@dashboard'
	]);

	Route::get('login', [
		'as' => 'login',
		'uses' => 'AdminController@login'
	]); 

	Route::post('signin', [
		'as' => 'signin',
		'uses' => 'AdminController@signin'
	]); 

	Route::get('signout', [
		'as' => 'signout',
		'uses' => 'AdminController@signout'
	]); 

	Route::group(['prefix' => '/dashboard/'],function(){
		Route::get('add', [
			'as' => 'addAdmin',
			'uses' => 'AdminController@addAdmin'
		]); 

		Route::post('set', [
			'as' => 'setAdmin',
			'uses' => 'AdminController@setAdmin'
		]); 

		Route::get('edit/{id}', [
			'as' => 'editAdmin',
			'uses' => 'AdminController@editAdmin'
		]); 

		Route::post('update/{id}', [
			'as' => 'updateAdmin',
			'uses' => 'AdminController@updateAdmin'
		]); 

		Route::get('delete/{id}', [
			'as' => 'deleteAdmin',
			'uses' => 'AdminController@deleteAdmin'
		]); 
		
	});

	Route::group(['prefix' => '/user'],function(){

		Route::get('/', [
			'as' => 'activeUser',
			'uses' => 'AdminController@activeUser'
		]); 

		Route::get('/detail/{id}', [
			'as' => 'detailUser',
			'uses' => 'AdminController@detailUser'
		]); 


		Route::get('/delete/{id}', [
			'as' => 'deleteUser',
			'uses' => 'AdminController@deleteUser'
		]); 
		
	});

	Route::group(['prefix' => '/banneduser'],function(){

		Route::get('/', [
		'as' => 'bannedUser',
		'uses' => 'AdminController@bannedUser'
		]); 

		Route::get('/detail/{id}', [
			'as' => 'detailUser',
			'uses' => 'AdminController@detailBannedUser'
		]); 


		Route::get('/restore/{id}', [
			'as' => 'restoreUser',
			'uses' => 'AdminController@restoreUser'
		]); 
		
	});

	Route::group(['prefix' => '/stat'],function(){
		Route::get('/', [
			'as' => 'stat',
			'uses' => 'AdminController@stat'
		]); 
		Route::get('/add', [
			'as' => 'addStat',
			'uses' => 'AdminController@addStat'
		]); 

		Route::post('/set', [
			'as' => 'setStat',
			'uses' => 'AdminController@setStat'
		]); 

		Route::get('/edit/{id}', [
			'as' => 'editStat',
			'uses' => 'AdminController@editStat'
		]); 

		Route::post('/update/{id}', [
			'as' => 'updateStat',
			'uses' => 'AdminController@updateStat'
		]); 

		Route::get('/delete/{id}', [
			'as' => 'deleteStat',
			'uses' => 'AdminController@deleteStat'
		]); 
		
	});

	Route::get('/contact', [
		'as' => 'contact',
		'uses' => 'AdminController@contact'
	]); 

	Route::post('/contact/update', [
		'as' => 'updateContact',
		'uses' => 'AdminController@updateContact'
	]);

	Route::get('/version', [
		'as' => 'version',
		'uses' => 'AdminController@version'
	]); 

	Route::post('/version/update', [
		'as' => 'updateVersion',
		'uses' => 'AdminController@updateVersion'
	]); 

	Route::group(['prefix' => '/tutorial'],function(){
		Route::get('/', [
			'as' => 'tutorial',
			'uses' => 'AdminController@listTutorial'
		]); 
		Route::get('/add', [
			'as' => 'addTutorial',
			'uses' => 'AdminController@addTutorial'
		]); 

		Route::post('/set', [
			'as' => 'setTutorial',
			'uses' => 'AdminController@setTutorial'
		]); 

		Route::get('/edit/{id}', [
			'as' => 'editTutorial',
			'uses' => 'AdminController@editTutorial'
		]); 

		Route::post('/update/{id}', [
			'as' => 'updateTutorial',
			'uses' => 'AdminController@updateTutorial'
		]); 

		Route::get('/delete/{id}', [
			'as' => 'deleteTutorial',
			'uses' => 'AdminController@deleteTutorial'
		]); 
		
	});

});




























//client
//login client
Route::post('/api/v1/clients/login', 'API\clientAPIController@login');
//register client
Route::post('/api/v1/clients/register', 'API\clientAPIController@register');
//subscribe client
Route::post('/api/v1/clients/subscribe', 'API\clientAPIController@subscribe');
//change password
Route::post('/api/v1/clients/changepassword', 'API\clientAPIController@changePassword');
//client get signal
Route::post('/api/v1/clients/getSignal', 'API\clientAPIController@getSignal');
//client get signal by id
Route::post('/api/v1/clients/getsignalbyId', 'API\clientAPIController@getsignalbyId');
//getTutorial
Route::post('/api/v1/clients/getTutorial', 'API\clientAPIController@getTutorial');
//getTutorialbyId
Route::post('/api/v1/clients/getTutorialbyId', 'API\clientAPIController@getTutorialbyId');
//updateGcmId
Route::post('/api/v1/clients/updateGcmId','API\clientAPIController@updateGcmId');
//client done
//admin
//login admin
Route::post('/api/v1/admins/login', 'API\adminAPIController@login');
//set Admin
Route::post('/api/v1/admins/setAdmin', 'API\adminAPIController@setAdmin');
//getAdmibyId
Route::post('/api/v1/admins/getAdmibyId', 'API\adminAPIController@getAdmibyId');
//delete admin 
Route::post('/api/v1/admins/deleteAdmin', 'API\adminAPIController@deleteAdmin');
//set signal 
Route::post('/api/v1/admins/setSignal', 'API\adminAPIController@setSignal');
//update signal
Route::post('/api/v1/admins/updateSignal', 'API\adminAPIController@updateSignal');
//delete signal
Route::post('/api/v1/admins/deleteSignal', 'API\adminAPIController@deleteSignal');
//get client
Route::post('/api/v1/admins/getClient', 'API\adminAPIController@getClient');
//getClientById
Route::post('/api/v1/admins/getClientById', 'API\adminAPIController@getClientById');
//getClientSubscribe
Route::post('/api/v1/admins/getClientSubscribe', 'API\adminAPIController@getClientSubscribe');
//deleteClient
Route::post('/api/v1/admins/deleteClient', 'API\adminAPIController@deleteClient');
//setContact
Route::post('/api/v1/admins/setContact', 'API\adminAPIController@setContact');
//deleteContact
Route::post('/api/v1/admins/deleteContact', 'API\adminAPIController@deleteContact');
//updateContact
Route::post('/api/v1/admins/updateContact', 'API\adminAPIController@updateContact');
//getSignal
Route::post('/api/v1/admins/getSignal', 'API\adminAPIController@getSignal');
//getSignalbyId
Route::post('/api/v1/admins/getSignalbyId', 'API\adminAPIController@getSignalbyId');
//deleteSignal
Route::post('/api/v1/admins/deleteSignal', 'API\adminAPIController@deleteSignal');
//setTutorial
Route::post('/api/v1/admins/setTutorial', 'API\adminAPIController@setTutorial');
//updateTutorial
Route::post('/api/v1/admins/updateTutorial', 'API\adminAPIController@updateTutorial');
//getTutorial
Route::post('/api/v1/admins/getTutorial', 'API\adminAPIController@getTutorial');
//getTutorialbyId
Route::post('/api/v1/admins/getTutorialbyId', 'API\adminAPIController@getTutorialbyId');
//deleteTutorial
Route::post('/api/v1/admins/deleteTutorial', 'API\adminAPIController@deleteTutorial');
//setNote
Route::post('/api/v1/admins/setNote', 'API\adminAPIController@setNote');
//admin done

//global
//signal get
Route::get('/api/v1/signals/getSignal', 'API\signalAPIController@getSignal');
Route::get('/api/v1/tutorials/getTutorial', 'API\tutorialAPIController@getTutorial');
Route::get('/api/v1/getContact','API\contactAPIController@index');
Route::get('/api/v1/getVersion','API\versionAPIController@getVersion');
Route::post('/api/v1/setVersion','API\versionAPIController@setVersion');
//global end
// Route::post('/api/v1/clients/auth', 'API\clientAPIController@authenticate');
/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

// Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
//     Route::group(['prefix' => 'v1'], function () {
//         require config('infyom.laravel_generator.path.api_routes');
//     });
// });


