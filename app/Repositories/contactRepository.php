<?php

namespace App\Repositories;

use App\Models\contact;
use InfyOm\Generator\Common\BaseRepository;

class contactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'phone',
        'email',
        'address',
        'facebookLink',
        'twitterLink',
        'googleLink'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return contact::class;
    }
}
