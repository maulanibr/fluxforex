<?php

namespace App\Repositories;

use App\Models\signal;
use InfyOm\Generator\Common\BaseRepository;
use Carbon\Carbon;

class signalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'published',
        'currencyPair',
        'dailyTrend',
        'mode',
        'entryLevel'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return signal::class;
    }

    public function whereYear($column,$operand, $year){
        $this->model = $this->model->whereYear($column,$operand, $year);
        return $this;
    }

    // public function whereNotnull($column,$yearmonth){

    //     $this->model = $this->model->where(Carbon::parse('published')->month,'>=','"'.$yearmonth.'"')->whereNotNull($column)->orderBy('published','desc');
    //     return $this;
    // }

    public function whereNotnullbyType($column,$type){

        $this->model = $this->model->where('currencyPair','=',$type)->whereNotNull($column)->orderBy('published','desc');
        return $this;
    }
   
}
