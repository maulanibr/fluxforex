<?php

namespace App\Repositories;

use App\Models\Version;
use InfyOm\Generator\Common\BaseRepository;

class VersionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'versionApp'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Version::class;
    }
}
