<?php

namespace App\Repositories;

use App\Models\admin;
use InfyOm\Generator\Common\BaseRepository;

class adminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'privilege'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return admin::class;
    }
}
