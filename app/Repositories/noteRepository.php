<?php

namespace App\Repositories;

use App\Models\note;
use InfyOm\Generator\Common\BaseRepository;

class noteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'content'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return note::class;
    }
}
