<?php

namespace App\Repositories;

use App\Models\tutorial;
use InfyOm\Generator\Common\BaseRepository;

class tutorialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'content',
        'image',
        'thumbnail',
        'author',
        'writen'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tutorial::class;
    }
}
