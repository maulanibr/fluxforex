<?php

namespace App\Repositories;

use App\Models\client;
use InfyOm\Generator\Common\BaseRepository;

class clientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'country',
        'phone',
        'registered',
        'endSubscribe',
        'gcmId'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return client::class;
    }

    public function withTrashed() {
        $this->model = $this->model->withTrashed();
        return $this;
    }

    public function onlyTrashed(){
        $this->model = $this->model->onlyTrashed()->orderBy('deleted_at','desc');
        return $this;

    }

    public function orderBy($column, $direction = 'desc'){
        $this->model = $this->model->orderBy($column, $direction = 'desc');
        return $this;
    }

    public function restore(){
        $this->model = $this->model->withTrashed()->restore();
        return $this;
    }
}
