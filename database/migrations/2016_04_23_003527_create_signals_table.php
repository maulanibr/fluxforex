<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatesignalsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');
            $table->date('published');
            $table->string('currencyPair');
            $table->string('dailyTrend');
            $table->string('mode');
            $table->string('entryLevel');
            $table->string('TP1');
            $table->string('TP2');
            $table->string('TP3');
            $table->string('SL');
            $table->string('note');
            $table->string('triggeredAt');
            $table->string('closedAt');
            $table->string('profit');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('signals');
    }
}
