<?php

use Faker\Factory as Faker;
use App\Models\contact;
use App\Repositories\contactRepository;

trait MakecontactTrait
{
    /**
     * Create fake instance of contact and save it in database
     *
     * @param array $contactFields
     * @return contact
     */
    public function makecontact($contactFields = [])
    {
        /** @var contactRepository $contactRepo */
        $contactRepo = App::make(contactRepository::class);
        $theme = $this->fakecontactData($contactFields);
        return $contactRepo->create($theme);
    }

    /**
     * Get fake instance of contact
     *
     * @param array $contactFields
     * @return contact
     */
    public function fakecontact($contactFields = [])
    {
        return new contact($this->fakecontactData($contactFields));
    }

    /**
     * Get fake data of contact
     *
     * @param array $postFields
     * @return array
     */
    public function fakecontactData($contactFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'phone' => $fake->word,
            'email' => $fake->word,
            'address' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $contactFields);
    }
}
