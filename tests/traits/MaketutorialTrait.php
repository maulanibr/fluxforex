<?php

use Faker\Factory as Faker;
use App\Models\tutorial;
use App\Repositories\tutorialRepository;

trait MaketutorialTrait
{
    /**
     * Create fake instance of tutorial and save it in database
     *
     * @param array $tutorialFields
     * @return tutorial
     */
    public function maketutorial($tutorialFields = [])
    {
        /** @var tutorialRepository $tutorialRepo */
        $tutorialRepo = App::make(tutorialRepository::class);
        $theme = $this->faketutorialData($tutorialFields);
        return $tutorialRepo->create($theme);
    }

    /**
     * Get fake instance of tutorial
     *
     * @param array $tutorialFields
     * @return tutorial
     */
    public function faketutorial($tutorialFields = [])
    {
        return new tutorial($this->faketutorialData($tutorialFields));
    }

    /**
     * Get fake data of tutorial
     *
     * @param array $postFields
     * @return array
     */
    public function faketutorialData($tutorialFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'content' => $fake->word,
            'image' => $fake->word,
            'thumbnail' => $fake->word,
            'author' => $fake->word,
            'writen' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $tutorialFields);
    }
}
