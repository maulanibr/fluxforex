<?php

use Faker\Factory as Faker;
use App\Models\admin;
use App\Repositories\adminRepository;

trait MakeadminTrait
{
    /**
     * Create fake instance of admin and save it in database
     *
     * @param array $adminFields
     * @return admin
     */
    public function makeadmin($adminFields = [])
    {
        /** @var adminRepository $adminRepo */
        $adminRepo = App::make(adminRepository::class);
        $theme = $this->fakeadminData($adminFields);
        return $adminRepo->create($theme);
    }

    /**
     * Get fake instance of admin
     *
     * @param array $adminFields
     * @return admin
     */
    public function fakeadmin($adminFields = [])
    {
        return new admin($this->fakeadminData($adminFields));
    }

    /**
     * Get fake data of admin
     *
     * @param array $postFields
     * @return array
     */
    public function fakeadminData($adminFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'password' => $fake->word,
            'privilege' => $fake->word,
            'token' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $adminFields);
    }
}
