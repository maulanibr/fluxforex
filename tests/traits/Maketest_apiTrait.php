<?php

use Faker\Factory as Faker;
use App\Models\test_api;
use App\Repositories\test_apiRepository;

trait Maketest_apiTrait
{
    /**
     * Create fake instance of test_api and save it in database
     *
     * @param array $testApiFields
     * @return test_api
     */
    public function maketest_api($testApiFields = [])
    {
        /** @var test_apiRepository $testApiRepo */
        $testApiRepo = App::make(test_apiRepository::class);
        $theme = $this->faketest_apiData($testApiFields);
        return $testApiRepo->create($theme);
    }

    /**
     * Get fake instance of test_api
     *
     * @param array $testApiFields
     * @return test_api
     */
    public function faketest_api($testApiFields = [])
    {
        return new test_api($this->faketest_apiData($testApiFields));
    }

    /**
     * Get fake data of test_api
     *
     * @param array $postFields
     * @return array
     */
    public function faketest_apiData($testApiFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'password' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $testApiFields);
    }
}
