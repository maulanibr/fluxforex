<?php

use Faker\Factory as Faker;
use App\Models\t1;
use App\Repositories\t1Repository;

trait Maket1Trait
{
    /**
     * Create fake instance of t1 and save it in database
     *
     * @param array $t1Fields
     * @return t1
     */
    public function maket1($t1Fields = [])
    {
        /** @var t1Repository $t1Repo */
        $t1Repo = App::make(t1Repository::class);
        $theme = $this->faket1Data($t1Fields);
        return $t1Repo->create($theme);
    }

    /**
     * Get fake instance of t1
     *
     * @param array $t1Fields
     * @return t1
     */
    public function faket1($t1Fields = [])
    {
        return new t1($this->faket1Data($t1Fields));
    }

    /**
     * Get fake data of t1
     *
     * @param array $postFields
     * @return array
     */
    public function faket1Data($t1Fields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'password' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $t1Fields);
    }
}
