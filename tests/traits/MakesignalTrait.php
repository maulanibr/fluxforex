<?php

use Faker\Factory as Faker;
use App\Models\signal;
use App\Repositories\signalRepository;

trait MakesignalTrait
{
    /**
     * Create fake instance of signal and save it in database
     *
     * @param array $signalFields
     * @return signal
     */
    public function makesignal($signalFields = [])
    {
        /** @var signalRepository $signalRepo */
        $signalRepo = App::make(signalRepository::class);
        $theme = $this->fakesignalData($signalFields);
        return $signalRepo->create($theme);
    }

    /**
     * Get fake instance of signal
     *
     * @param array $signalFields
     * @return signal
     */
    public function fakesignal($signalFields = [])
    {
        return new signal($this->fakesignalData($signalFields));
    }

    /**
     * Get fake data of signal
     *
     * @param array $postFields
     * @return array
     */
    public function fakesignalData($signalFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'published' => $fake->word,
            'currencyPair' => $fake->word,
            'dailyTrend' => $fake->word,
            'mode' => $fake->word,
            'entryLevel' => $fake->word,
            'TP1' => $fake->word,
            'TP2' => $fake->word,
            'TP3' => $fake->word,
            'SL' => $fake->word,
            'note' => $fake->word,
            'triggeredAt' => $fake->word,
            'closedAt' => $fake->word,
            'profit' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $signalFields);
    }
}
