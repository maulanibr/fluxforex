<?php

use App\Models\tutorial;
use App\Repositories\tutorialRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class tutorialRepositoryTest extends TestCase
{
    use MaketutorialTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var tutorialRepository
     */
    protected $tutorialRepo;

    public function setUp()
    {
        parent::setUp();
        $this->tutorialRepo = App::make(tutorialRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatetutorial()
    {
        $tutorial = $this->faketutorialData();
        $createdtutorial = $this->tutorialRepo->create($tutorial);
        $createdtutorial = $createdtutorial->toArray();
        $this->assertArrayHasKey('id', $createdtutorial);
        $this->assertNotNull($createdtutorial['id'], 'Created tutorial must have id specified');
        $this->assertNotNull(tutorial::find($createdtutorial['id']), 'tutorial with given id must be in DB');
        $this->assertModelData($tutorial, $createdtutorial);
    }

    /**
     * @test read
     */
    public function testReadtutorial()
    {
        $tutorial = $this->maketutorial();
        $dbtutorial = $this->tutorialRepo->find($tutorial->id);
        $dbtutorial = $dbtutorial->toArray();
        $this->assertModelData($tutorial->toArray(), $dbtutorial);
    }

    /**
     * @test update
     */
    public function testUpdatetutorial()
    {
        $tutorial = $this->maketutorial();
        $faketutorial = $this->faketutorialData();
        $updatedtutorial = $this->tutorialRepo->update($faketutorial, $tutorial->id);
        $this->assertModelData($faketutorial, $updatedtutorial->toArray());
        $dbtutorial = $this->tutorialRepo->find($tutorial->id);
        $this->assertModelData($faketutorial, $dbtutorial->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletetutorial()
    {
        $tutorial = $this->maketutorial();
        $resp = $this->tutorialRepo->delete($tutorial->id);
        $this->assertTrue($resp);
        $this->assertNull(tutorial::find($tutorial->id), 'tutorial should not exist in DB');
    }
}
