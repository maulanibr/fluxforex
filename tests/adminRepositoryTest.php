<?php

use App\Models\admin;
use App\Repositories\adminRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class adminRepositoryTest extends TestCase
{
    use MakeadminTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var adminRepository
     */
    protected $adminRepo;

    public function setUp()
    {
        parent::setUp();
        $this->adminRepo = App::make(adminRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateadmin()
    {
        $admin = $this->fakeadminData();
        $createdadmin = $this->adminRepo->create($admin);
        $createdadmin = $createdadmin->toArray();
        $this->assertArrayHasKey('id', $createdadmin);
        $this->assertNotNull($createdadmin['id'], 'Created admin must have id specified');
        $this->assertNotNull(admin::find($createdadmin['id']), 'admin with given id must be in DB');
        $this->assertModelData($admin, $createdadmin);
    }

    /**
     * @test read
     */
    public function testReadadmin()
    {
        $admin = $this->makeadmin();
        $dbadmin = $this->adminRepo->find($admin->id);
        $dbadmin = $dbadmin->toArray();
        $this->assertModelData($admin->toArray(), $dbadmin);
    }

    /**
     * @test update
     */
    public function testUpdateadmin()
    {
        $admin = $this->makeadmin();
        $fakeadmin = $this->fakeadminData();
        $updatedadmin = $this->adminRepo->update($fakeadmin, $admin->id);
        $this->assertModelData($fakeadmin, $updatedadmin->toArray());
        $dbadmin = $this->adminRepo->find($admin->id);
        $this->assertModelData($fakeadmin, $dbadmin->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteadmin()
    {
        $admin = $this->makeadmin();
        $resp = $this->adminRepo->delete($admin->id);
        $this->assertTrue($resp);
        $this->assertNull(admin::find($admin->id), 'admin should not exist in DB');
    }
}
