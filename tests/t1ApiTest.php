<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class t1ApiTest extends TestCase
{
    use Maket1Trait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatet1()
    {
        $t1 = $this->faket1Data();
        $this->json('POST', '/api/v1/t1s', $t1);

        $this->assertApiResponse($t1);
    }

    /**
     * @test
     */
    public function testReadt1()
    {
        $t1 = $this->maket1();
        $this->json('GET', '/api/v1/t1s/'.$t1->id);

        $this->assertApiResponse($t1->toArray());
    }

    /**
     * @test
     */
    public function testUpdatet1()
    {
        $t1 = $this->maket1();
        $editedt1 = $this->faket1Data();

        $this->json('PUT', '/api/v1/t1s/'.$t1->id, $editedt1);

        $this->assertApiResponse($editedt1);
    }

    /**
     * @test
     */
    public function testDeletet1()
    {
        $t1 = $this->maket1();
        $this->json('DELETE', '/api/v1/t1s/'.$t1->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/t1s/'.$t1->id);

        $this->assertResponseStatus(404);
    }
}
