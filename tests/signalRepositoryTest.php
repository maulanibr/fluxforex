<?php

use App\Models\signal;
use App\Repositories\signalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class signalRepositoryTest extends TestCase
{
    use MakesignalTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var signalRepository
     */
    protected $signalRepo;

    public function setUp()
    {
        parent::setUp();
        $this->signalRepo = App::make(signalRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatesignal()
    {
        $signal = $this->fakesignalData();
        $createdsignal = $this->signalRepo->create($signal);
        $createdsignal = $createdsignal->toArray();
        $this->assertArrayHasKey('id', $createdsignal);
        $this->assertNotNull($createdsignal['id'], 'Created signal must have id specified');
        $this->assertNotNull(signal::find($createdsignal['id']), 'signal with given id must be in DB');
        $this->assertModelData($signal, $createdsignal);
    }

    /**
     * @test read
     */
    public function testReadsignal()
    {
        $signal = $this->makesignal();
        $dbsignal = $this->signalRepo->find($signal->id);
        $dbsignal = $dbsignal->toArray();
        $this->assertModelData($signal->toArray(), $dbsignal);
    }

    /**
     * @test update
     */
    public function testUpdatesignal()
    {
        $signal = $this->makesignal();
        $fakesignal = $this->fakesignalData();
        $updatedsignal = $this->signalRepo->update($fakesignal, $signal->id);
        $this->assertModelData($fakesignal, $updatedsignal->toArray());
        $dbsignal = $this->signalRepo->find($signal->id);
        $this->assertModelData($fakesignal, $dbsignal->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletesignal()
    {
        $signal = $this->makesignal();
        $resp = $this->signalRepo->delete($signal->id);
        $this->assertTrue($resp);
        $this->assertNull(signal::find($signal->id), 'signal should not exist in DB');
    }
}
