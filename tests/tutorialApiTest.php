<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class tutorialApiTest extends TestCase
{
    use MaketutorialTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatetutorial()
    {
        $tutorial = $this->faketutorialData();
        $this->json('POST', '/api/v1/tutorials', $tutorial);

        $this->assertApiResponse($tutorial);
    }

    /**
     * @test
     */
    public function testReadtutorial()
    {
        $tutorial = $this->maketutorial();
        $this->json('GET', '/api/v1/tutorials/'.$tutorial->id);

        $this->assertApiResponse($tutorial->toArray());
    }

    /**
     * @test
     */
    public function testUpdatetutorial()
    {
        $tutorial = $this->maketutorial();
        $editedtutorial = $this->faketutorialData();

        $this->json('PUT', '/api/v1/tutorials/'.$tutorial->id, $editedtutorial);

        $this->assertApiResponse($editedtutorial);
    }

    /**
     * @test
     */
    public function testDeletetutorial()
    {
        $tutorial = $this->maketutorial();
        $this->json('DELETE', '/api/v1/tutorials/'.$tutorial->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/tutorials/'.$tutorial->id);

        $this->assertResponseStatus(404);
    }
}
