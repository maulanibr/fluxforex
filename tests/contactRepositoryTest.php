<?php

use App\Models\contact;
use App\Repositories\contactRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class contactRepositoryTest extends TestCase
{
    use MakecontactTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var contactRepository
     */
    protected $contactRepo;

    public function setUp()
    {
        parent::setUp();
        $this->contactRepo = App::make(contactRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatecontact()
    {
        $contact = $this->fakecontactData();
        $createdcontact = $this->contactRepo->create($contact);
        $createdcontact = $createdcontact->toArray();
        $this->assertArrayHasKey('id', $createdcontact);
        $this->assertNotNull($createdcontact['id'], 'Created contact must have id specified');
        $this->assertNotNull(contact::find($createdcontact['id']), 'contact with given id must be in DB');
        $this->assertModelData($contact, $createdcontact);
    }

    /**
     * @test read
     */
    public function testReadcontact()
    {
        $contact = $this->makecontact();
        $dbcontact = $this->contactRepo->find($contact->id);
        $dbcontact = $dbcontact->toArray();
        $this->assertModelData($contact->toArray(), $dbcontact);
    }

    /**
     * @test update
     */
    public function testUpdatecontact()
    {
        $contact = $this->makecontact();
        $fakecontact = $this->fakecontactData();
        $updatedcontact = $this->contactRepo->update($fakecontact, $contact->id);
        $this->assertModelData($fakecontact, $updatedcontact->toArray());
        $dbcontact = $this->contactRepo->find($contact->id);
        $this->assertModelData($fakecontact, $dbcontact->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletecontact()
    {
        $contact = $this->makecontact();
        $resp = $this->contactRepo->delete($contact->id);
        $this->assertTrue($resp);
        $this->assertNull(contact::find($contact->id), 'contact should not exist in DB');
    }
}
