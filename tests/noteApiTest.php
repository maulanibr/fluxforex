<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class noteApiTest extends TestCase
{
    use MakenoteTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatenote()
    {
        $note = $this->fakenoteData();
        $this->json('POST', '/api/v1/notes', $note);

        $this->assertApiResponse($note);
    }

    /**
     * @test
     */
    public function testReadnote()
    {
        $note = $this->makenote();
        $this->json('GET', '/api/v1/notes/'.$note->id);

        $this->assertApiResponse($note->toArray());
    }

    /**
     * @test
     */
    public function testUpdatenote()
    {
        $note = $this->makenote();
        $editednote = $this->fakenoteData();

        $this->json('PUT', '/api/v1/notes/'.$note->id, $editednote);

        $this->assertApiResponse($editednote);
    }

    /**
     * @test
     */
    public function testDeletenote()
    {
        $note = $this->makenote();
        $this->json('DELETE', '/api/v1/notes/'.$note->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/notes/'.$note->id);

        $this->assertResponseStatus(404);
    }
}
