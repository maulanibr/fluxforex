<?php

use App\Models\test_api;
use App\Repositories\test_apiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class test_apiRepositoryTest extends TestCase
{
    use Maketest_apiTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var test_apiRepository
     */
    protected $testApiRepo;

    public function setUp()
    {
        parent::setUp();
        $this->testApiRepo = App::make(test_apiRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatetest_api()
    {
        $testApi = $this->faketest_apiData();
        $createdtest_api = $this->testApiRepo->create($testApi);
        $createdtest_api = $createdtest_api->toArray();
        $this->assertArrayHasKey('id', $createdtest_api);
        $this->assertNotNull($createdtest_api['id'], 'Created test_api must have id specified');
        $this->assertNotNull(test_api::find($createdtest_api['id']), 'test_api with given id must be in DB');
        $this->assertModelData($testApi, $createdtest_api);
    }

    /**
     * @test read
     */
    public function testReadtest_api()
    {
        $testApi = $this->maketest_api();
        $dbtest_api = $this->testApiRepo->find($testApi->id);
        $dbtest_api = $dbtest_api->toArray();
        $this->assertModelData($testApi->toArray(), $dbtest_api);
    }

    /**
     * @test update
     */
    public function testUpdatetest_api()
    {
        $testApi = $this->maketest_api();
        $faketest_api = $this->faketest_apiData();
        $updatedtest_api = $this->testApiRepo->update($faketest_api, $testApi->id);
        $this->assertModelData($faketest_api, $updatedtest_api->toArray());
        $dbtest_api = $this->testApiRepo->find($testApi->id);
        $this->assertModelData($faketest_api, $dbtest_api->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletetest_api()
    {
        $testApi = $this->maketest_api();
        $resp = $this->testApiRepo->delete($testApi->id);
        $this->assertTrue($resp);
        $this->assertNull(test_api::find($testApi->id), 'test_api should not exist in DB');
    }
}
