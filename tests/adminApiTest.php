<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class adminApiTest extends TestCase
{
    use MakeadminTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateadmin()
    {
        $admin = $this->fakeadminData();
        $this->json('POST', '/api/v1/admins', $admin);

        $this->assertApiResponse($admin);
    }

    /**
     * @test
     */
    public function testReadadmin()
    {
        $admin = $this->makeadmin();
        $this->json('GET', '/api/v1/admins/'.$admin->id);

        $this->assertApiResponse($admin->toArray());
    }

    /**
     * @test
     */
    public function testUpdateadmin()
    {
        $admin = $this->makeadmin();
        $editedadmin = $this->fakeadminData();

        $this->json('PUT', '/api/v1/admins/'.$admin->id, $editedadmin);

        $this->assertApiResponse($editedadmin);
    }

    /**
     * @test
     */
    public function testDeleteadmin()
    {
        $admin = $this->makeadmin();
        $this->json('DELETE', '/api/v1/admins/'.$admin->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/admins/'.$admin->id);

        $this->assertResponseStatus(404);
    }
}
