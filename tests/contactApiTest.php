<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class contactApiTest extends TestCase
{
    use MakecontactTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatecontact()
    {
        $contact = $this->fakecontactData();
        $this->json('POST', '/api/v1/contacts', $contact);

        $this->assertApiResponse($contact);
    }

    /**
     * @test
     */
    public function testReadcontact()
    {
        $contact = $this->makecontact();
        $this->json('GET', '/api/v1/contacts/'.$contact->id);

        $this->assertApiResponse($contact->toArray());
    }

    /**
     * @test
     */
    public function testUpdatecontact()
    {
        $contact = $this->makecontact();
        $editedcontact = $this->fakecontactData();

        $this->json('PUT', '/api/v1/contacts/'.$contact->id, $editedcontact);

        $this->assertApiResponse($editedcontact);
    }

    /**
     * @test
     */
    public function testDeletecontact()
    {
        $contact = $this->makecontact();
        $this->json('DELETE', '/api/v1/contacts/'.$contact->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/contacts/'.$contact->id);

        $this->assertResponseStatus(404);
    }
}
