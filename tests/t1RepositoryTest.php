<?php

use App\Models\t1;
use App\Repositories\t1Repository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class t1RepositoryTest extends TestCase
{
    use Maket1Trait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var t1Repository
     */
    protected $t1Repo;

    public function setUp()
    {
        parent::setUp();
        $this->t1Repo = App::make(t1Repository::class);
    }

    /**
     * @test create
     */
    public function testCreatet1()
    {
        $t1 = $this->faket1Data();
        $createdt1 = $this->t1Repo->create($t1);
        $createdt1 = $createdt1->toArray();
        $this->assertArrayHasKey('id', $createdt1);
        $this->assertNotNull($createdt1['id'], 'Created t1 must have id specified');
        $this->assertNotNull(t1::find($createdt1['id']), 't1 with given id must be in DB');
        $this->assertModelData($t1, $createdt1);
    }

    /**
     * @test read
     */
    public function testReadt1()
    {
        $t1 = $this->maket1();
        $dbt1 = $this->t1Repo->find($t1->id);
        $dbt1 = $dbt1->toArray();
        $this->assertModelData($t1->toArray(), $dbt1);
    }

    /**
     * @test update
     */
    public function testUpdatet1()
    {
        $t1 = $this->maket1();
        $faket1 = $this->faket1Data();
        $updatedt1 = $this->t1Repo->update($faket1, $t1->id);
        $this->assertModelData($faket1, $updatedt1->toArray());
        $dbt1 = $this->t1Repo->find($t1->id);
        $this->assertModelData($faket1, $dbt1->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletet1()
    {
        $t1 = $this->maket1();
        $resp = $this->t1Repo->delete($t1->id);
        $this->assertTrue($resp);
        $this->assertNull(t1::find($t1->id), 't1 should not exist in DB');
    }
}
