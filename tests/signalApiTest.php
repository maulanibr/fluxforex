<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class signalApiTest extends TestCase
{
    use MakesignalTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatesignal()
    {
        $signal = $this->fakesignalData();
        $this->json('POST', '/api/v1/signals', $signal);

        $this->assertApiResponse($signal);
    }

    /**
     * @test
     */
    public function testReadsignal()
    {
        $signal = $this->makesignal();
        $this->json('GET', '/api/v1/signals/'.$signal->id);

        $this->assertApiResponse($signal->toArray());
    }

    /**
     * @test
     */
    public function testUpdatesignal()
    {
        $signal = $this->makesignal();
        $editedsignal = $this->fakesignalData();

        $this->json('PUT', '/api/v1/signals/'.$signal->id, $editedsignal);

        $this->assertApiResponse($editedsignal);
    }

    /**
     * @test
     */
    public function testDeletesignal()
    {
        $signal = $this->makesignal();
        $this->json('DELETE', '/api/v1/signals/'.$signal->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/signals/'.$signal->id);

        $this->assertResponseStatus(404);
    }
}
