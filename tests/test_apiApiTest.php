<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class test_apiApiTest extends TestCase
{
    use Maketest_apiTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatetest_api()
    {
        $testApi = $this->faketest_apiData();
        $this->json('POST', '/api/v1/testApis', $testApi);

        $this->assertApiResponse($testApi);
    }

    /**
     * @test
     */
    public function testReadtest_api()
    {
        $testApi = $this->maketest_api();
        $this->json('GET', '/api/v1/testApis/'.$testApi->id);

        $this->assertApiResponse($testApi->toArray());
    }

    /**
     * @test
     */
    public function testUpdatetest_api()
    {
        $testApi = $this->maketest_api();
        $editedtest_api = $this->faketest_apiData();

        $this->json('PUT', '/api/v1/testApis/'.$testApi->id, $editedtest_api);

        $this->assertApiResponse($editedtest_api);
    }

    /**
     * @test
     */
    public function testDeletetest_api()
    {
        $testApi = $this->maketest_api();
        $this->json('DELETE', '/api/v1/testApis/'.$testApi->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/testApis/'.$testApi->id);

        $this->assertResponseStatus(404);
    }
}
