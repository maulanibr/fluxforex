 @extends('admin.shared.header')

 @section('page-content')
            <link href="{{ URL('/') }}/public/admins/res/css/edit.css"  rel="stylesheet" type="text/css"/>



 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                   <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">Tutorial</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Add </span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- <div class="row margin-top25">
                        <div class="col-md-6">
                            <div class="btn-group">

                                <button id="sample_editable_1_new" class="btn green"> 
                                    <i class="fa fa-arrow-left"></i>
                                    Back
                                </button>
                            </div>
                        </div>
                    </div> -->
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                       
                                  
                                
                            <div class="row margin-top25">
                                <div class="col-md-7">
                                    @if($errors->any())
                                         <div id="dialog-box" style="background-color:#FF6666;border:solid 1px #F3A8A8; width:300px; height:50px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:red"></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color:#2B2424;">
                                                <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                    <div class="portlet box purple  ">
                                        
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-plus"></i> Edit Tutorial 
                                            </div>
                                        </div>
                                        <div class="portlet-body form " style="display: block;">
                                            <form name="Form-Forex" method="post" enctype="multipart/form-data" action="{{URL('/')}}/admin/tutorial/update/{{ $tutorial->id }}" class="form-horizontal" role="form">
                                                <div class="form-body">
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Title</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Title.." name="title" value="{{ $tutorial->title }}" required/>
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Content</label>
                                                        <div class="col-md-5">
                                                            <textarea rows="5" class="form-control input-sm" placeholder="...." name="content" required>{!! $tutorial->content !!}</textarea>
                                                           
                                                        </div>
                                                         
                                                         
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Author</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Author.." name="author" value="{{ $tutorial->author }}" required> 
                                                        </div>
                                                    </div>
                                                    
                                                   
                                                  
                                                <div class="form-actions right1">
                                                    <a href="{{URL('/')}}/admin/tutorial"><button type="button" class="btn default">Cancel</button></a>
                                                    <button type="submit" class="btn green">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                            <script>
                          
                                function validateForm(){
                                    
                                    var pwd1 = document.forms["Form-Forex"]["note"].value;
                                    

                                    
                                    if(pwd1.length<=200){
                                        return true;
                                    }else{
                                       
                                        $('#message_err').show();
                                       
                                        return false;
                                    }   
                                    

                                }

                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }                                
                               
                            </script>

                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@stop