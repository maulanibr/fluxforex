 @extends('admin.shared.header')

 @section('page-content')
             <link href="{{ URL('/') }}/public/admin/res/css/planning.css"  rel="stylesheet" type="text/css"/>

 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">Tutorial</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>List</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->

                    <!-- BEGIN TABLE-->
                         <div class="portlet light bordered margin-top50">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase">Tutorial</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                        <a href="{{ URL('/') }}/admin/tutorial/add">
                                            <button class="btn green"> Add New
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </a>
                                        </div>
                                    </div>
                                           
                                </div>
                                 @if($errors->any())
                                        <div id="dialog-box" class="col-md-6" style="background-color:#03C31A;border:solid 1px #9AEAA5; width:300px; height:40px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:green" ></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color: white;">
                                                    <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                <div class="portlet-body">

                                    <table class="table  table-bordered table-advance table-hover " width="100%" id="sample_1">
                                         <thead>
                                                <tr>
                                                    
                                                    <th>No </th>
                                                    <th> Date </th>
                                                    <th> Title </th>
                                                    <th> Content </th>
                                                    <th> Author </th>
                                                    
                                                   
                                                    <th colspan="2" align="center" > Action </th>
                                                    
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $count=1;
                                                    if(!empty($result)){
                                                    foreach($result as $rs){
                                                ?>
                                                <tr>
                                                   
                                                    <td ><?php echo $count++; ?></td>
                                                   
                                                    <td class="hidden-xs"><?php echo $rs['writen'] ?></td>
                                                    <td><?php echo $rs['title'] ?></td>
                                                    <td><?php echo mb_strimwidth($rs['content'], 0, 25, '...'); ?></td>
                                                    <td><?php echo $rs['author'] ?></td>
                                                  
                                                     <td>
                                                        <a href="{{URL("/")}}/admin/tutorial/edit/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle green btn-sm purple">
                                                        <i class="fa fa-share"></i></i> Edit </a>
                                                    </td>
                                                    <?php if(Session::get('privilege')=='super'){ ?>  
                                                    <td>
                                                        <a onclick="return preconfirm()" href="{{URL("/")}}/admin/tutorial/delete/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle dark btn-sm red">
                                                            <i class="fa fa-close"></i> Delete </a>
                                                    </td>
                                                    <?php } ?>
                                                    <!-- <td>
                                                        <a onclick="return preconfirm()" href="{{URL("/")}}/admin/stat/delete/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle dark btn-sm black">
                                                            <i class="fa fa-trash-o"></i> Delete </a>
                                                    </td> -->
                                                </tr>
                                                <?php }}else{ ?>
                                                <tr>
                                                   <p>No Signal found</p>
                                                </tr>
                                                <?php } ?>
                                                
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                    <!-- END Table-->

                              <script>
                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }

                                function preconfirm(){
                                    confirm('are you sure want to delete this tutorial?');
                                }                                
                               
                            </script>

                    <!-- END PAGE HEADER-->
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@stop