



<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Flux Forex | Content Management System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{URL('/')}}/public/admins/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="{{URL('/')}}/public/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" /> -->
        <link href="{{URL('/')}}/public/admins/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="{{URL('/')}}/public/admin/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" /> -->
        <link href="{{URL('/')}}/public/admins/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{URL('/')}}/public/admins/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL('/')}}/public/admins/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{URL('/')}}/public/admins/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{URL('/')}}/public/admins/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{URL('/')}}/public/admins/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL('/')}}/public/admins/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{URL('/')}}/public/admins/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />  <!-- CSS DHANA -->
        <link href="{{URL('/')}}/public/admins/res/css/login.css"  rel="stylesheet" type="text/css"/>
        
        <!-- CSS DHANA END-->
         <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{URL('/')}}/public/admins/assets/pages/login-2.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
         </head>
    <!-- END HEAD -->
    <body class=" login" id="grad1" >
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="{{URL('/')}}/public/admins/res/img/flux_forex_logo_white_2.png" style="height: 57px;" alt="" /> </a>
        </div>
     
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form method="post" enctype="multipart/form-data" action="{{URL('/')}}/admin/signin">
                <div class="form-title">
                    <span class="form-title">Welcome.</span>
                    <span class="form-subtitle">Please login.</span>
                </div>

				
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
               
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="name" required/> 
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required/> 
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block uppercase">Login</button>
                </div>
               
               	@if($errors->any())
				<div class="form-title " style="text-align: center;">
					<span  class="form-title">{{$errors->first()}}</span>
				</div>
				@endif
                <div class="login-options">
                    
                  
                </div>
              
            </form>
            <!-- END LOGIN FORM -->
           
        </div>
        <div class="copyright hide"> 2016 © Flux Forex </div>        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <!-- <script src="{{URL('/')}}/public/admin/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> -->
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
         <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{URL('/')}}/public/admins/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{URL('/')}}/public/admins/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{URL('/')}}/public/admins/assets/pages/scripts/table-datatables-editable.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{URL('/')}}/public/admins/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="{{URL('/')}}/public/admins/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>