 @extends('admin.shared.header')

 @section('page-content')
            <link href="{{ URL('/') }}/public/admin/res/css/edit.css"  rel="stylesheet" type="text/css"/>

 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">Signal</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Add </span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- <div class="row margin-top25">
                        <div class="col-md-6">
                            <div class="btn-group">

                                <button id="sample_editable_1_new" class="btn green"> 
                                    <i class="fa fa-arrow-left"></i>
                                    Back
                                </button>
                            </div>
                        </div>
                    </div> -->
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                       
                                  
                                
                            <div class="row margin-top25">
                                <div class="col-md-7">
                                    @if($errors->any())
                                         <div id="dialog-box" style="background-color:#FF6666;border:solid 1px #F3A8A8; width:300px; height:50px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:red"></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color:#2B2424;">
                                                <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                    <div class="portlet box purple  ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-plus"></i> Add Signal 
                                            </div>
                                        </div>
                                        <div class="portlet-body form " style="display: block;">
                                            <form name="Form-Forex" method="post" enctype="multipart/form-data" action="{{URL('/')}}/admin/stat/set" class="form-horizontal" role="form" onsubmit="return validateForm()">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">currency </label>
                                                        <div class="col-md-3">
                                                            <select class="form-control input-sm" name="currencyPair" required>
                                                                <option value="">------</option>
                                                                <option value="EUR/USD">EURUSD</option>
                                                                <option value="USD/JPY">USDJPY</option>
                                                                <option value="GBP/USD">GBPUSD</option>
                                                                <option value="USD/CHF">USDCHF</option>
                                                                <option value="EUR/GBP">EURGBP</option>
                                                                <option value="EUR/JPY">EURJPY</option>
                                                                <option value="EUR/CHF">EURCHF</option>
                                                                <option value="AUD/USD">AUDUSD</option>
                                                                <option value="USD/CAD">USDCAD</option>
                                                                <option value="NZD/USD">NZDUSD</option>
                                                                <option value="AUD/JPY">AUDJPY</option>
                                                                <option value="NZD/JPY">NZDJPY</option>
                                                                <option value="GBP/JPY">GBPJPY</option>
                                                                <option value="CADJPY">CADJPY</option>
                                                                <option value="EURAUD">EURAUD</option>
                                                                <option value="GBPAUD">GBPAUD</option>
                                                                <option value="GBPCHF">GBPCHF</option>
                                                                <option value="EURCAD">EURCAD</option>
                                                                <option value="EURNZD">EURNZD</option>
                                                                <option value="GBPCAD">GBPCAD</option>
                                                            </select>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Mode</label>
                                                        <div class="col-md-3">
                                                           <select class="form-control input-sm" name="mode" required>
                                                                <option value="">------</option>
                                                                <option value="Buy">Buy</option>
                                                                <option value="Sell">Sell</option>
                                                                <option value="No Trade">No Trade</option>
                                                                <option value="Cancel">Cancel</option>
                                                                <option value="Close">Close</option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Today's Trend</label>
                                                        <div class="col-md-3">
                                                            <select class="form-control input-sm" name="dailyTrend" required>
                                                                <option value="">------</option>
                                                                <option>Up</option>
                                                                <option>Neutral</option>
                                                                <option>Down</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">TP 1</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="TP 1...." name="TP1" required  /> 
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                            <label class="col-md-3 control-label">TP 2</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control input-sm" placeholder="TP 2.." name="TP2" required /> 
                                                            </div>
                                                    </div>
                                                    
                                                     <div class="form-group">
                                                        <label class="col-md-3 control-label">TP 3</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="TP 3.." name="TP3" required /> 
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">SL</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="SL.." name="SL" required /> 
                                                        </div>
                                                       <!--  <div class="form-group">
                                                            <label class="col-md-3 control-label">SL 2</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control input-sm" placeholder="SL 2.." required> 
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Trigger</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Trigger.." name="triggeredAt" required/>
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Close</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Close.." name="closedAt" required/> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Profit</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Profit.." name="profit" required> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Note</label>
                                                        <div class="col-md-5">
                                                            <textarea rows="5" class="form-control input-sm" placeholder="...." name="note" required></textarea>
                                                            <!-- <input type="text" class="form-control input-sm" placeholder="Profit.." name="profit" required>  -->
                                                        </div>
                                                         <p> max 200 char </p>
                                                         <div id="message_err" class="col-md-3" style="display: none;color:red;">
                                                            <i class="fa fa-close"></i> exceed 200 char
                                                        </div>
                                                    </div>
                                                   
                                                  
                                                <div class="form-actions right1">
                                                    <a href="{{URL('/')}}/admin/stat"><button type="button" class="btn default">Cancel</button></a>
                                                    <button type="submit" class="btn green">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                            <script>
                          
                                function validateForm(){
                                    
                                    var pwd1 = document.forms["Form-Forex"]["note"].value;
                                    

                                    
                                    if(pwd1.length<=200){
                                        return true;
                                    }else{
                                       
                                        $('#message_err').show();
                                       
                                        return false;
                                    }   
                                    

                                }

                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }                                
                               
                            </script>

                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@stop