 @extends('admin.shared.header')

 @section('page-content')
            <link href="{{ URL('/') }}/public/admins/res/css/edit.css"  rel="stylesheet" type="text/css"/>



 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">Signal</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Edit </span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- <div class="row margin-top25">
                        <div class="col-md-6">
                            <div class="btn-group">

                                <button id="sample_editable_1_new" class="btn green"> 
                                    <i class="fa fa-arrow-left"></i>
                                    Back
                                </button>
                            </div>
                        </div>
                    </div> -->
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                       
                                  
                                
                            <div class="row margin-top25">
                                <div class="col-md-7">
                                    @if($errors->any())
                                         <div id="dialog-box" style="background-color:#FF6666;border:solid 1px #F3A8A8; width:300px; height:50px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:red"></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color:#2B2424;">
                                                <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                    <div class="portlet box purple  ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-plus"></i> Edit Signal 
                                            </div>
                                        </div>
                                        <div class="portlet-body form " style="display: block;">
                                            <form name="Form-Forex" method="post" enctype="multipart/form-data" action="{{URL('/')}}/admin/stat/update/<?php echo $result['id'] ?>" class="form-horizontal" role="form" onsubmit="return validateForm()">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">currency </label>
                                                        <div class="col-md-3">
                                                            <select class="form-control input-sm" name="currencyPair">
                                                                <option value="" >------</option>
                                                                <option value="EUR - USD" <?php if($result['currencyPair']=='EUR - USD'){echo 'selected';} ?>>EURUSD</option>
                                                                <option value="USD - JPY" <?php if($result['currencyPair']=='USD - JPY'){echo 'selected';} ?>>USDJPY</option>
                                                                <option value="GBP - USD" <?php if($result['currencyPair']=='GBP - USD'){echo 'selected';} ?>>GBPUSD</option>
                                                                <option value="USD - CHF" <?php if($result['currencyPair']=='USD - CHF'){echo 'selected';} ?>>USDCHF</option>
                                                                <option value="EUR - GBP" <?php if($result['currencyPair']=='EUR - GBP'){echo 'selected';} ?>>EURGBP</option>
                                                                <option value="EUR - JPY" <?php if($result['currencyPair']=='EUR - JPY'){echo 'selected';} ?>>EURJPY</option>
                                                                <option value="EUR - CHF" <?php if($result['currencyPair']=='EUR - CHF'){echo 'selected';} ?>>EURCHF</option>
                                                                <option value="AUD - USD" <?php if($result['currencyPair']=='AUD - USD'){echo 'selected';} ?>>AUDUSD</option>
                                                                <option value="USD - CAD" <?php if($result['currencyPair']=='USD - CAD'){echo 'selected';} ?>>USDCAD</option>
                                                                <option value="NZD - USD" <?php if($result['currencyPair']=='NZD - USD'){echo 'selected';} ?>>NZDUSD</option>
                                                                <option value="AUD - JPY" <?php if($result['currencyPair']=='AUD - JPY'){echo 'selected';} ?>>AUDJPY</option>
                                                                <option value="NZD - JPY" <?php if($result['currencyPair']=='NZD - JPY'){echo 'selected';} ?>>NZDJPY</option>
                                                                <option value="GBP - JPY" <?php if($result['currencyPair']=='GBP - JPY'){echo 'selected';} ?>>GBPJPY</option>
                                                                <option value="CAD - JPY" <?php if($result['currencyPair']=='CAD - JPY'){echo 'selected';} ?>>CADJPY</option>
                                                                <option value="EUR - AUD" <?php if($result['currencyPair']=='EUR - AUD'){echo 'selected';} ?>>EURAUD</option>
                                                                <option value="GBP - AUD" <?php if($result['currencyPair']=='GBP - AUD'){echo 'selected';} ?>>GBPAUD</option>
                                                                <option value="GBP - CHF" <?php if($result['currencyPair']=='GBP - CHF'){echo 'selected';} ?>>GBPCHF</option>
                                                                <option value="EUR - CAD" <?php if($result['currencyPair']=='EUR - CAD'){echo 'selected';} ?>>EURCAD</option>
                                                                <option value="EUR - NZD" <?php if($result['currencyPair']=='EUR - NZD'){echo 'selected';} ?>>EURNZD</option>
                                                                <option value="GBP - CAD" <?php if($result['currencyPair']=='GBP - CAD'){echo 'selected';} ?>>GBPCAD</option>
                                                            </select>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Mode</label>
                                                        <div class="col-md-3">
                                                           <select class="form-control input-sm" name="mode">
                                                                <option value="">------</option>
                                                                <option value="BUY" <?php if(strtolower($result['mode'])=='buy'){echo 'selected';} ?>>Buy</option>
                                                                <option value="SELL" <?php if(strtolower($result['mode'])=='sell'){echo 'selected';} ?>>Sell</option>
                                                                <option value="NO TRADE" <?php if(strtolower($result['mode'])=='no trade'){echo 'selected';} ?>>No Trade</option>
                                                                <option value="CANCEL" <?php if(strtolower($result['mode'])=='cancel'){echo 'selected';} ?>>Cancel</option>
                                                                <option value="CLOSE" <?php if(strtolower($result['mode'])=='close'){echo 'selected';} ?>>Close</option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="col-md-3 control-label">Entry</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="TP 1...." value="<?php echo $result['entryLevel']; ?>" name="entry"  /> 
                                                        </div>
                                                        
                                                    </div>
                                                   <!--  <div class="form-group">
                                                        <label class="col-md-3 control-label">Today's Trend</label>
                                                        <div class="col-md-3">
                                                            <select class="form-control input-sm" name="dailyTrend">
                                                                <option value="">------</option>
                                                                <option value="Up" <?php if($result['dailyTrend']=='Up'){echo 'selected';} ?>>Up</option>
                                                                <option value="Neutral" <?php if($result['dailyTrend']=='Neutral'){echo 'selected';} ?>>Neutral</option>
                                                                <option value="Down" <?php if($result['dailyTrend']=='Down'){echo 'selected';} ?>>Down</option>
                                                            </select>
                                                        </div>
                                                    </div> -->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">TP 1</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="TP 1...." value="<?php echo $result['TP1']; ?>" name="TP1"  /> 
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                            <label class="col-md-3 control-label">TP 2</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control input-sm" placeholder="TP 2.." value="<?php echo $result['TP2']; ?>" name="TP2"  /> 
                                                            </div>
                                                    </div>
                                                    
                                                     <div class="form-group">
                                                        <label class="col-md-3 control-label">TP 3</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="TP 3.." value="<?php echo $result['TP3']; ?>" name="TP3"  /> 
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">SL</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="SL.." value="<?php echo $result['SL']; ?>" name="SL"  /> 
                                                        </div>
                                                       <!--  <div class="form-group">
                                                            <label class="col-md-3 control-label">SL 2</label>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control input-sm" placeholder="SL 2.." required> 
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <label class="col-md-3 control-label">Trigger</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Trigger.." value="<?php echo $result['triggeredAt']; ?>" name="triggeredAt" />
                                                         </div>
                                                    </div> -->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Close</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Close.." value="<?php echo $result['closedAt']; ?>" name="closedAt" /> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Profit</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" placeholder="Profit.." value="<?php echo $result['profit']; ?>" name="profit"> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Note</label>
                                                        <div class="col-md-5">
                                                            <textarea rows="5" class="form-control input-sm" placeholder="...." name="note" ><?php echo $result['note']; ?></textarea>
                                                            <!-- <input type="text" class="form-control input-sm" placeholder="Profit.." name="profit" required>  -->
                                                        </div>
                                                         <p> max 200 char </p>
                                                         <div id="message_err" class="col-md-3" style="display: none;color:red;">
                                                            <i class="fa fa-close"></i> exceed 200 char
                                                        </div>
                                                    </div>
                                                   
                                                  
                                                <div class="form-actions right1">
                                                    <a href="{{URL('/')}}/admin/stat"><button type="button" class="btn default">Cancel</button></a>
                                                    <button type="submit" class="btn green">Post</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                            <script>
                          
                                function validateForm(){
                                    
                                    var pwd1 = document.forms["Form-Forex"]["note"].value;
                                    

                                    
                                    if(pwd1.length<=200){
                                        return true;
                                    }else{
                                       
                                        $('#message_err').show();
                                       
                                        return false;
                                    }   
                                    

                                }

                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }                                
                               
                            </script>

                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@stop