 @extends('admin.shared.header')

 @section('page-content')
             <link href="{{ URL('/') }}/public/admin/res/css/planning.css"  rel="stylesheet" type="text/css"/>

 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">Stat</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>List</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->

                    <!-- BEGIN TABLE-->
                         <div class="portlet light bordered margin-top50">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase">History Signal</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                               <!--  <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                        <a href="{{ URL('/') }}/admin/stat/add">
                                            <button class="btn green"> Add New
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </a>
                                        </div>
                                    </div>
                                           
                                </div> -->
                                 @if($errors->any())
                                        <div id="dialog-box" class="col-md-6" style="background-color:#03C31A;border:solid 1px #9AEAA5; width:300px; height:40px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:green" ></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color: white;">
                                                    <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                <div class="portlet-body">

                                    <table class="table  table-bordered table-advance table-hover " width="100%" id="sample_1">
                                         <thead>
                                                <tr>
                                                    
                                                    <th>No </th>
                                                    <th> Time </th>
                                                    <th> Currency </th>
                                                    <th> Mode </th>
                                                    <th> Entry </th>
                                                   <!--  <th> Trend </th> -->
                                                    <th> TP 1 </th>
                                                    <th> TP 2 </th>
                                                    <th> TP 3 </th>
                                                    <th> SL  </th>
                                                    <!-- <th> SL 2 </th> -->
                                                    <!-- <th> Trigger Point </th> -->
                                                    <th> Closing Point </th>
                                                    <th> Profit Point </th>
                                                    <th colspan="2" > Action </th>
                                                    
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $count=1;
                                                    if(!empty($result)){
                                                    foreach($result as $rs){
                                                ?>
                                                <tr>
                                                   
                                                    <td ><?php echo $count++; ?></td>
                                                   
                                                    <td class="hidden-xs"><?php echo $rs['published'] ?></td>
                                                    <td><?php echo $rs['currencyPair'] ?></td>
                                                    <td><?php echo $rs['mode'] ?></td>
                                                    <td><?php echo $rs['entryLevel'] ?></td>
                                                  
                                                    <td><?php echo $rs['TP1'] ?></td>
                                                    <td><?php echo $rs['TP2'] ?></td>
                                                    <td><?php echo $rs['TP3'] ?></td>
                                                    <td><?php echo $rs['SL'] ?></td>
                                                   
                                                 
                                                    <td><?php echo $rs['closedAt'] ?></td>
                                                    <td><?php echo $rs['profit'] ?></td>
                                                    
                                                     <td>
                                                        <a href="{{URL("/")}}/admin/stat/edit/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle green btn-sm purple">
                                                        <i class="fa fa-share"></i></i> Edit </a>
                                                    </td>
                                                    <!-- <td>
                                                        <a onclick="return preconfirm()" href="{{URL("/")}}/admin/stat/delete/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle dark btn-sm black">
                                                            <i class="fa fa-trash-o"></i> Delete </a>
                                                    </td> -->
                                                </tr>
                                                <?php }}else{ ?>
                                                <tr>
                                                   <p>No Signal found</p>
                                                </tr>
                                                <?php } ?>
                                                
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                    <!-- END Table-->

                              <script>
                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }

                                function preconfirm(){
                                    confirm('are you sure want to delete this admin?');
                                }                                
                               
                            </script>

                    <!-- END PAGE HEADER-->
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@stop