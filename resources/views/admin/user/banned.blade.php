  @extends('admin.shared.header')

 @section('page-content')
             <link href="{{ URL('/') }}/public/admin/res/css/planning.css"  rel="stylesheet" type="text/css"/>

 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">User</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Banned User</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->

                    <!-- BEGIN TABLE-->
                         <div class="portlet light bordered margin-top50">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase">LIST BANNED USER</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                
                                  @if($errors->any())
                                        <div id="dialog-box" class="col-md-6" style="background-color:#03C31A;border:solid 1px #9AEAA5; width:300px; height:40px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:green" ></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color: white;">
                                                    <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                <div class="portlet-body">

                                    <table class="table  table-bordered table-advance table-hover " width="100%" id="sample_1">
                                         <thead>
                                                <tr>
                                                    
                                                    <th>No </th>
                                                    <th> Name </th>
                                                    <th> E-mail </th>
                                                    <th> Mobile Phone </th>
                                                    <th> Detail </th>
                                                    <th> Restore </th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php 
                                                    $count=1;
                                                    if(!empty($result)){
                                                    foreach($result as $rs){
                                                ?>
                                                <tr>
                                                   
                                                    <td ><?php echo $count++; ?></td>
                                                   
                                                    <td class="hidden-xs"><?php echo $rs['name'] ?></td>
                                                    <td><?php echo $rs['email'] ?></td>
                                                    <td><?php echo $rs['phone']; ?></td>
                                                    
                                                    
                                                     <td>
                                                        <a href="{{URL("/")}}/admin/banneduser/detail/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle green btn-sm purple">
                                                        <i class="fa fa-share"></i></i> View </a>
                                                    </td>
                                                    <td>
                                                        <a onclick="return preconfirm()" href="{{URL("/")}}/admin/banneduser/restore/<?php echo $rs["id"] ?>" class="btn btn-outline btn-circle dark btn-sm blue">
                                                            <i class="fa fa-upload"></i> Restore </a>
                                                    </td>
                                                </tr>
                                                <?php }}else{ ?>
                                                <tr>
                                                   <p>No User found</p>
                                                </tr>
                                                <?php } ?>
                                                
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                    <!-- END Table-->
                     <script>
                        function myFunction() {
                            $('#dialog-box').hide();   
                        }

                        function preconfirm(){
                            confirm('are you sure want to restore this user?');
                        }                                
                               
                    </script>

                    <!-- END PAGE HEADER-->
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@stop