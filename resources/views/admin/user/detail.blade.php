@extends('admin.shared.header')

 @section('page-content')
<link href="{{URL('/') }}/public/admin/res/css/edit.css"  rel="stylesheet" type="text/css"/>
<?php 
    function checkStatus($date){
                    $now = Carbon\Carbon::now()->format('Y-m-d');
                    $date = new Carbon\Carbon($date);
                    $date = $date->format('Y-m-d');
                    
                    if(empty($date) || $now > $date){
                        return '------';
                    }else{
                        return $date;
                    }

    }
?>
 

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="index.html">User</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Detail </span>
                            </li>
                        </ul>
                       
                    </div>
                     
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                        <div class="portlet light bordered margin-top50">
                            <div class="portlet-title">
                                <?php if(empty($result['deleted_at'])){ ?>
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase">Active User</span>
                                </div>
                                <div class="tools"> </div>
                                <?php }else{ ?>
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase">Banned User</span>
                                </div>
                                <div class="tools"> </div>
                                <?php } ?>
                            </div>
                                  
                            <div class="row margin-top50">
                                <div class="col-md-7">
                                    <div class="portlet box purple  ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-edit"></i> Detail User
                                            </div>
                                        </div>
                                        <div class="portlet-body form " style="display: block;">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Name : </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">{{$result['name']}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Email : </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">{{$result['email']}}</label>
                                                        </div>
                                                    </div>
                                                   
                                                     <div class="form-group">
                                                        <label class="col-md-3 control-label">Country : </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">{{$result['country']}}</label>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="col-md-3 control-label">Phone Number : </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">{{$result['phone']}} </label>
                                                        </div>
                                                    </div>
                                                    <!--  <div class="form-group">
                                                        <label class="col-md-3 control-label">Password : </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">dhkshdkas </label>
                                                        </div>
                                                    </div> -->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Register Date : </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">{{$result['registered']}}</label>
                                                        </div>
                                                    </div>
                                                    <?php if(empty($result['deleted_at'])){ ?>
                                                     <div class="form-group">
                                                        <label class="col-md-3 control-label">End Subscribe : </label>
                                                        <div class="col-md-9">
                                                          
                                                          <label class=" control-label"><?php echo checkStatus($result['endSubscribe']) ?>
                                                          </label>
                                                         
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <!--  <div class="form-group">
                                                        <label class="col-md-3 control-label">GCMid: </label>
                                                        <div class="col-md-9">
                                                          <label class=" control-label">isn89908 </label>
                                                        </div>
                                                    </div> -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                     
                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>

@stop