@extends('admin.shared.header')

 @section('page-content')
            <link href="{{ URL('/') }}/public/admins/res/css/edit.css"  rel="stylesheet" type="text/css"/>
<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="">Contact</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Edit</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                            
                                  
                            <div class="row margin-top50">
                                <div class="col-md-7">
                                     @if($errors->any())
                                        <div id="dialog-box" style="background-color:#FF6666;border:solid 1px #F3A8A8; width:300px; height:50px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:red"></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color:#2B2424;">
                                                <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                       @endif

                                       @if (session('message'))
                                       <div id="dialog-box" style="background-color:#03C31A;border:solid 1px #9AEAA5; width:300px; height:40px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:green" ></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color: white;">
                                                    <i class="fa fa-warning"> {{ session('message')}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                        @endif
                                     
                                    <div class="portlet box purple  ">
                                        <div class="portlet-title">
                                           
                                            <div class="caption">
                                                <i class="fa fa-gift"></i> Edit Contact
                                            </div>



                                        </div>
                                        <div class="portlet-body form " style="display: block;">
                                            <form name="Form-Forex" method="post" enctype="multipart/form-data" action="{{URL('/')}}/admin/contact/update" class="form-horizontal" role="form">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Facebook Link</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control input-sm" placeholder="link" name="facebook" value="<?php echo $result[0]['facebookLink'] ?>" /> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Twitter Link</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control input-sm" placeholder="link" name="twitter" value="<?php echo $result[0]['twitterLink'] ?>" /> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Google+ Link</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control input-sm" placeholder="link" name="google" value="<?php echo $result[0]['googleLink'] ?>" /> 
                                                        </div>
                                                    </div>
                                                  
                                                    <div class="form-group">
                                                        <label class="col-md-6">  
                                                             <p style="font-style: italic;
                                                            font-size: 11px;">*if there is no link, please type '#' in input text form</p>
                                                        </label>
                                                     </div>
                                                  
                                                    <div class="form-actions right1">
                                                        <!-- <a href="{{URL('/')}}/admin">
                                                            <button type="button" class="btn default">Cancel</button>
                                                        </a> -->
                                                        <button type="submit" class="btn green">Update</button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                            <script>

                             function myFunction() {
                                    $('#dialog-box').hide();   
                             }

                             // function validateForm(){
                                    
                             //        var pwd1 = document.forms["Form-Forex"]["password"].value;
                             //        var pwd2 = document.forms["Form-Forex"]["cfpwd"].value;
                                    
                             //        if(pwd1==pwd2){
                             //            return true;
                             //        }else{
                                       
                             //            $('#message_err').show();
                                       
                             //            return false;
                             //        }   
                                    

                             //    }       

                            </script>
                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

@stop