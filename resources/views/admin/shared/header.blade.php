<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Flux Forex | CMS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="icon" href="{{URL('/')}}/public/img/logo-icon/green/24px-green.png">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ URL('/') }}/public/admins/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
       <!--  <link href="{{ URL('/') }}/public/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" /> -->
        <link href="{{ URL('/') }}/public/admins/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
       <!--  <link href="{{ URL('/') }}/public/admin/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" /> -->
        <link href="{{ URL('/') }}/public/admins/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ URL('/') }}/public/admins/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ URL('/') }}/public/admins/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ URL('/') }}/public/admins/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ URL('/') }}/public/admins/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ URL('/') }}/public/admins/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ URL('/') }}/public/admins/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ URL('/') }}/public/admins/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />  <!-- CSS DHANA -->
       
        <link href="{{ URL('/') }}/public/admins/res/css/style.css"  rel="stylesheet" type="text/css"/>
         <script src="{{ URL('/') }}/public/admins/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <!-- CSS DHANA END-->
         </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div id="grad1" class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ URL('/') }}/admin">
                        <img src="{{ URL('/') }}/public/admins/res/img/flux_forex_logo_white_2.png" alt="logo" class="logo-default logo" /> </a>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right" style="">
                        
                        
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a  class="dropdown-toggle" >
                                <!-- <img alt="" class="img-circle" src="{{ URL('/') }}/public/admin/res/img/iconUser.jpg" /> -->
                                <span class="username username-hide-on-mobile"> Welcome, <?php echo Session::get('name'); ?> </span>
                            </a>
                            
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                       
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->

        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <li class="sidebar-search-wrapper">
                            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                            
                            <!-- END RESPONSIVE QUICK SEARCH FORM -->
                        </li>
                         
                        <li class="heading">
                            <h3 class="uppercase">Navigation Bar</h3>
                        </li>

                        <li class="nav-item start ">
                            <a href="{{ URL('/') }}/admin" class="nav-link nav-toggle">
                                <i class="fa fa-home "></i>
                                <span class="title">Administrator</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="#" class="nav-link nav-toggle">
                               <i class="fa fa-desktop"></i>
                                 <span class="title">User</span>
                            </a>
                            <ul class="sub-menu">
                              
                                <li class="nav-item  ">
                                   <a href="{{ URL('/') }}/admin/user">
                                        <i class="fa fa-tags"></i>
                                        <span class="title">User Active</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{ URL('/') }}/admin/banneduser">
                                        <i class="fa fa-thumb-tack"></i>
                                        <span class="title">User Banned</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                         <li class="nav-item  ">
                            <a href="{{ URL('/') }}/admin/stat" class="nav-link nav-toggle">
                                <i class="fa fa-bar-chart-o"></i>
                                <span class="title">Signal</span>
                                
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{{ URL('/') }}/admin/tutorial" class="nav-link nav-toggle">
                                <i class="fa fa-bar-chart-o"></i>
                                <span class="title">Tutorial</span>
                                
                            </a>
                        </li>
                         <li class="nav-item  ">
                            <a href="{{ URL('/') }}/admin/contact" class="nav-link nav-toggle">
                                <i class="fa fa-phone"></i>
                                <span class="title">Contact </span>
                                
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{{ URL('/') }}/admin/version" class="nav-link nav-toggle">
                                <i class="fa fa-book"></i>
                                <span class="title">Version App</span>
                                
                            </a>
                        </li>
                        
                        <li class="nav-item  margin-top350">
                            <a href="{{URL('/')}}/admin/signout" class="nav-link nav-toggle log-out">
                               <i class="fa fa-power-off log-out-icon"></i>
                                 <span class="title">Log Out</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->

            @yield('page-content')

            <!-- BEGIN FOOTER -->
        <div class="page-footer"  >
            <div class="page-footer-inner"> 2016 &copy; Wijaya Vision.
               
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
       
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
       <!--  <script src="{{ URL('/') }}/public/admin/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> -->
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
         <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ URL('/') }}/public/admins/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ URL('/') }}/public/admins/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL('/') }}/public/admins/assets/pages/scripts/table-datatables-editable.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ URL('/') }}/public/admins/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/public/admins/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>