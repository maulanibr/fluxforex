 @extends('admin.shared.header')

 @section('page-content')
             <link href="{{ URL('/') }}/public/admin/res/css/planning.css"  rel="stylesheet" type="text/css"/>
 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="{{ url('/') }}/admin/">Administrator</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>List</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                        
                        <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        LIST ADMINISTRATOR
                                    </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                        <?php if(Session::get('privilege')=='super'){ ?>
                                        <div class="btn-group">
                                            <a href="{{ URL('/') }}/admin/dashboard/add">
                                                <button class="btn green"> Add New
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <?php } ?>
                                        
                                    </div>
                                     @if($errors->any())
                                        <div id="dialog-box" class="col-md-6" style="background-color:#03C31A;border:solid 1px #9AEAA5; width:300px; height:40px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:green" ></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color: white;">
                                                    <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                  </div>
                                  <div class="row">
                                <div class="portlet-body col-md-6 nopadding-side">
                                    <div class="table-scrollable ">
                                        <table  class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-user"></i>Username
                                                    </th>
                                                    <th class="hidden-xs">
                                                        Privilege
                                                    </th>
                                                   <!--  <th>
                                                        <i class="fa fa-briefcase"></i>Password</th> -->
                                                    <?php if(Session::get('privilege')=='super'){ ?>    
                                                    <th colspan="2" width="100"> Action </th>
                                                    <?php }else{ ?>
                                                     <th width="100"> Action </th>
                                                     <?php } ?>
                                                    <!-- <th> Delete </th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                $count = 1;
                                                foreach($admin as $adm){


                                             ?>
                                                <tr>
                                                    <td ><?php echo $count ?></td>
                                                    <td class="hidden-xs"> <?php echo $adm["name"]; ?> </td>
                                                    <td class="hidden-xs"> <?php echo $adm["privilege"]; ?> </td>
                                                   <!--  <td> 2560.60$ </td> -->
                                                    <td>
                                                        <a href="{{URL("/")}}/admin/dashboard/edit/<?php echo $adm["id"] ?>" class="btn btn-outline btn-circle green btn-sm purple">
                                                            <i class="fa fa-share"></i></i> Edit </a>
                                                    </td>
                                                    <?php if(Session::get('privilege')=='super'){ ?>   
                                                    <td>
                                                        <a onclick="return preconfirm()" href="{{URL("/")}}/admin/dashboard/delete/<?php echo $adm["id"] ?>" class="btn btn-outline btn-circle dark btn-sm black">
                                                            <i class="fa fa-trash-o"></i> Delete </a>
                                                    </td>
                                                    <?php }?>
                                                </tr>
                                             <?php $count++;} ?>  
                                              
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                            <script>
                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }

                                function preconfirm(){
                                    confirm('are you sure want to delete this admin?');
                                }                                
                               
                            </script>
                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

@stop