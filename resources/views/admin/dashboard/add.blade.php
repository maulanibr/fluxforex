@extends('admin.shared.header')

 @section('page-content')
            <link href="{{ URL('/') }}/public/admin/res/css/edit.css"  rel="stylesheet" type="text/css"/>
<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper margin-top5">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content ">
                    <!-- BEGIN PAGE HEADER-->
                  
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span href="">Administrator</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Add</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN TABLE-->
                            
                                  
                            <div class="row margin-top50">
                                <div class="col-md-7">
                                     @if($errors->any())
                                         <div id="dialog-box" style="background-color:#FF6666;border:solid 1px #F3A8A8; width:300px; height:50px;" >
                                            <div style="width:100%; text-align:right;">  
                                                <a onclick="myFunction()"><i class="fa fa-close" style="color:red"></i></a>
                                            </div>
                                            <div style="width:100%;margin-top: -30px;padding-left: 5px; ">
                                                <p style="color:#2B2424;">
                                                <i class="fa fa-warning"> {{$errors->first()}}</i>
                                                </p>
                                            </div>    
                                        </div>
                                     @endif
                                    <div class="portlet box purple  ">
                                        <div class="portlet-title">
                                           
                                            <div class="caption">
                                                <i class="fa fa-gift"></i> Add New  Administrator 
                                            </div>



                                        </div>
                                        <div class="portlet-body form " style="display: block;">
                                            <form name="Form-Forex" method="post" enctype="multipart/form-data" action="{{URL('/')}}/admin/dashboard/set" class="form-horizontal" role="form" onsubmit="return validateForm()">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Username</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control input-sm" placeholder="name" name="name" required/> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Password</label>
                                                        <div class="col-md-6">
                                                            <input id="pwd1" type="password"  class="form-control input-sm" placeholder="password" name="password" required/> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Confirm Password</label>
                                                        <div class="col-md-6">
                                                            <input id="pwd2" type="password" class="form-control input-sm" placeholder="password" name="cfpwd" required/> 
                                                        </div>
                                                        <div id="message_err" class="col-md-3" style="display: none;color:red;">
                                                            <i class="fa fa-close"></i> password not same
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Privilege</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control input-sm" name="privilege" required>
                                                                <option value="">-----</option>
                                                                <option value="admin">Admin</option>
                                                                <option value="super">Super Admin</option>
                                                              
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                                  
                                                <div class="form-actions right1">
                                                    <a href="{{URL('/')}}/admin">
                                                        <button type="button" class="btn default">Cancel</button>
                                                    </a>
                                                    <button type="submit" class="btn green">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- END Table-->
                    <!-- END PAGE HEADER-->
                   
                            

                           <script>
                          
                                function validateForm(){
                                    
                                    var pwd1 = document.forms["Form-Forex"]["password"].value;
                                    var pwd2 = document.forms["Form-Forex"]["cfpwd"].value;
                                    
                                    if(pwd1==pwd2){
                                        return true;
                                    }else{
                                       
                                        $('#message_err').show();
                                       
                                        return false;
                                    }   
                                    

                                }

                                function myFunction() {
                                    $('#dialog-box').hide();   
                                }                                
                               
                            </script>

                            
                     
                                 
                </div>
            </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

@stop