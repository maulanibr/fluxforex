

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<meta name="application-name" content="Flux Forex" />
	<meta name="keywords" content="Forex,Foreign Exchange,Flux,Flux Forex,Simplified,Trading,Broadcaster,Signal" />
	<meta http-equiv="refresh" content="300">

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="icon" href="{{URL('/')}}/public/img/logo-icon/green/24px-green.png">
	<link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ URL::asset('public/style.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ URL::asset('public/css/page/home.css') }}" type="text/css" />
	<!-- One Page Module Specific Stylesheet -->
	<link rel="stylesheet" href="{{ URL::asset('public/onepage.css') }}" type="text/css" />
	<!-- / -->

	<link rel="stylesheet" href="{{ URL::asset('public/css/dark.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ URL::asset('public/css/font-icons.css') }}" type="text/css" />
	<!-- <link rel="stylesheet" href="{{ URL::asset('public/css/et-line.css') }}" type="text/css" /> -->
	<link rel="stylesheet" href="{{ URL::asset('public/css/animate.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ URL::asset('public/css/magnific-popup.css') }}" type="text/css" />

	<link rel="stylesheet" href="{{ URL::asset('public/css/fonts.css') }}" type="text/css" />

	<link rel="stylesheet" href="{{ URL::asset('public/css/responsive.css') }}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="{{ URL::asset('public/js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('public/js/plugins.js') }}"></script>

	<!-- Document Title
	============================================= -->
	<title>Flux Forex | Trading Signal Broadcaster</title>

</head>

<body class="stretched">
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		 <header id="header" class="full-header transparent-header dark static-sticky" data-sticky-class="not-dark" data-sticky-offset="full" data-sticky-offset-negative="100">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="{{ url('/') }}" class="standard-logo" data-dark-logo="{{ URL::asset('public/img/flux_forex_logo_white_2.png') }}">
                        	<img src="{{ URL::asset('public/img/logoSection.png') }}" alt="Canvas Logo" class="padding5">
                        </a>
                        <a href="{{ url('/') }}" class="retina-logo" data-dark-logo="  {{ URL::asset('public/img/flux_forex_logo_white_2.png') }}">
                       		<img src="{{URL::asset('public/img/logoSection.png') }}" alt="Canvas Logo" class="padding5">
                        </a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
                            <li><a href="{{ url('/') }}" data-href=""><div>Home</div></a></li>
                            <li><a href="{{ url('/') }}/performance" data-href=""><div>Performance</div></a></li>
                            <!-- <li><a href="{{ url('/') }}/signal" data-href=""><div>Performance</div></a></li>
                            <li><a href="{{ url('/') }}/stat" data-href=""><div>Stat</div></a></li> -->
                            <!-- <li><a href="{{ url('/') }}/tutorial" data-href=""><div>Tutorial</div></a></li> -->
                            <li><a href="{{ url('/') }}/contact" data-href=""><div>Contact</div></a></li>
                        </ul>


                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

	@yield('page-content')


	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

<script type="text/javascript" src="{{ URL::asset('public/js/functions.js') }}"></script>

</body>
</html>