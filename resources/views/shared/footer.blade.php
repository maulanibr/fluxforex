 <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark bg-footer">

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix ">
                    <div class="col-md-6 divcenter center">
                         <div class="center clearfix inline-block ">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-gplus">
                                <i class="icon-gplus"></i>
                                <i class="icon-gplus"></i>
                            </a>

                        </div>
                        <br>
                        <div class="copyright-links"><a href="#">@Flux Forex 2016</a> </div>
                    </div>
                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->