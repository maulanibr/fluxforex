@extends('shared.header-green')

@section('page-content') 
         <link rel="stylesheet" href="{{ URL::asset('public/css/page/contact.css') }}"  type="text/css" />
     <!-- Slider
        ============================================= -->
        <section id="content" >

            <div class="content-wrap notoppadding nobottompadding">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class=" nobottommargin clearfix ">
                        <div class="col-md-12  ">
                            <div class="col-md-6 topmargin-lg center"><img src="{{ URL::asset('public/img/img-contact.png') }}"> </div>
                             <div class="col-md-6">
                                <h3 class="topmargin-sm">Get In Touch</h3>

                                <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

                                <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post" novalidate="novalidate">

                                    <div class="form-process"></div>

                                    <div class="col_one_third">
                                        <label for="template-contactform-name">Name <small>*</small></label>
                                        <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" aria-required="true">
                                    </div>

                                    <div class="col_one_third">
                                        <label for="template-contactform-email">Email <small>*</small></label>
                                        <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" aria-required="true">
                                    </div>

                                    <div class="col_one_third col_last">
                                        <label for="template-contactform-phone">Phone</label>
                                        <input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control">
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_three_third">
                                        <label for="template-contactform-subject">Subject <small>*</small></label>
                                        <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" aria-required="true">
                                    </div>

                                   

                                    <div class="clear"></div>

                                    <div class="col_full">
                                        <label for="template-contactform-message">Message <small>*</small></label>
                                        <textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30" aria-required="true"></textarea>
                                    </div>

                                    <div class="col_full hidden">
                                        <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control">
                                    </div>

                                    <div class="col_full right">
                                        <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit" style="background-color:#01c515; ">Send Message</button>
                                    </div>

                                </form>

                                <script type="text/javascript">

                                    $("#template-contactform").validate({
                                        submitHandler: function(form) {
                                            $('.form-process').fadeIn();
                                            $(form).ajaxSubmit({
                                                target: '#contact-form-result',
                                                success: function() {
                                                    $('.form-process').fadeOut();
                                                    $('#template-contactform').find('.sm-form-control').val('');
                                                    $('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
                                                    SEMICOLON.widget.notifications($('#contact-form-result'));
                                                }
                                            });
                                        }
                                    });

                                </script>

                  
                             </div>
                            <div class="col-md-12 center">
                                <h1  class="title-section topmargin-md center nobottommargin" >
                                    <span class="text-rotater nocolor " >
                                        <span class="t-rotate t700 font-body opm-large-word ">Find Us On </span>
                                    </span>
                                </h1>
                                <div class="center clearfix inline-block topmargin-sm ">
                                <a href="<?php echo $result[0]['facebookLink'] ?> " target="_blank" class="social-icon si-small si-borderless si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                 <div class="social-icon nomargin lineheight30 si-borderless ">
                                    <span class="middot">·</span>
                                </div>

                                <a href="<?php echo $result[0]['twitterLink'] ?> " target="_blank" class="social-icon si-small si-borderless si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <div class="social-icon nomargin lineheight30 si-borderless ">
                                    <span class="middot">·</span>
                                </div>

                                <a href="<?php echo $result[0]['googleLink'] ?> " target="_blank" class="social-icon si-small si-borderless si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>

                                </div>
                            </div>
                        <br>
                        <div class="copyright-links col-md-12 center topmargin-sm"><a href="#">@Flux Forex 2016</a> </div>
                        </div>

                    </div><!-- .postcontent end -->

                </div>

            </div>

        </section>

@stop