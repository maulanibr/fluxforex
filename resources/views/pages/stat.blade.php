

@extends('shared.header-green')

@section('page-content')
         <link rel="stylesheet" href="{{ URL::asset('public/css/page/stat.css') }}" type="text/css" />
    <!-- Slider
        ============================================= -->
        <section id="content" class="min-height520">

            <div class="content-wrap ">

                <div class="col-md-12 padding-side clearfix">
                    

                    <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12" id="lineChart1" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">EUR-USD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"></p><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas1" width="1261" height="300"></canvas>
                    </div>

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart2" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">USD-JPY</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 topmargin" style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas2" width="1261" height="300"></canvas>
                    </div>


                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart3" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-USD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas3" width="1261" height="300"></canvas>
                    </div>

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart4" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">USD-CHF</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas4" width="1261" height="300"></canvas>
                    </div>   

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart5" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">EUR-GBP</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas5" width="1261" height="300"></canvas>
                    </div>    

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart6" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">EUR-JPY</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas6" width="1261" height="300"></canvas>
                    </div>     

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart7" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">EUR-CHF</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas7" width="1261" height="300"></canvas>
                    </div>     

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart8" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">AUD-USD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas8" width="1261" height="300"></canvas>
                    </div>     

                     <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart9" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">USD-CAD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas9" width="1261" height="300"></canvas>
                    </div>     

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart10" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">NZD-USD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas10" width="1261" height="300"></canvas>
                    </div>   

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart11" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">AUD-JPY</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas11" width="1261" height="300"></canvas>
                    </div>   

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart12" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">NZD-JPY</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas12" width="1261" height="300"></canvas>
                    </div>   

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart13" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-JPY</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas13" width="1261" height="300"></canvas>
                    </div>    

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart14" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">CAD-JPY</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas14" width="1261" height="300"></canvas>
                    </div>    

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart15" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">EUR-AUD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas15" width="1261" height="300"></canvas>
                    </div>    

                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart16" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-AUD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas16" width="1261" height="300"></canvas>
                    </div>    


                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart17" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-AUD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas17" width="1261" height="300"></canvas>
                    </div>    


                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart18" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-AUD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas18" width="1261" height="300"></canvas>
                    </div>    


                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart19" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-AUD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas19" width="1261" height="300"></canvas>
                    </div>    


                      <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12 topmargin" id="lineChart20" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">GBP-AUD</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="lineChartCanvas20" width="1261" height="300"></canvas>
                    </div>    


                   <script type="text/javascript">

                    jQuery(window).load( function(){

                        var globalGraphSettings = {animation : Modernizr.canvas};
                        
                        var lineChartData1 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [<?php foreach($eurusd as $uj){
                                                // $last = end($eurusd);
                                               if($uj==end($eurusd)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>]
                                }
                               
                            ]
                        };

                       

                        var lineChartData2 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($usdjpy as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($usdjpy)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData3 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($gbpusd as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($gbpusd)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData4 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($usdchf as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($usdchf)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData5 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($eurgbp as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($eurgbp)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData6 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($eurjpy as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($eurjpy)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData7 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($eurchf as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($eurchf)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData8 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($audusd as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($audusd)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData9 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($usdcad as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($usdcad)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData10 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($nzdusd as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($nzdusd)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData11 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($audjpy as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($audjpy)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData12 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($nzdjpy as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($nzdjpy)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData13 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($gbpjpy as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($gbpjpy)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData14 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($cadjpy as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($cadjpy)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData15 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($euraud as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($euraud)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData16 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($gbpaud as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($gbpaud)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData17 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($gbpchf as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($gbpchf)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData18 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($eurcad as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($eurcad)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData19 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($eurnzd as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($eurnzd)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        var lineChartData20 = {
                            labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                            datasets : [
                                {
                                    fillColor : "rgba(220,220,220,0.5)",
                                    strokeColor : "#01c515",
                                    pointColor : "#01c515",
                                    pointStrokeColor : "#01c515",
                                    data : [
                                            <?php foreach($gbpcad as $uj){
                                                // $last = end($eurusd);
                                                if($uj==end($gbpcad)){
                                                    echo $uj;
                                                }else{
                                                    echo $uj.',';
                                                }
                                            } ?>

                                           ]
                                }
                               
                            ]
                        };

                        // function showLineChart(){
                            
                        //     var ctx = document.getElementById("lineChartCanvas").getContext("2d");
                        //     new Chart(ctx).Line(lineChartData,globalGraphSettings);

                        // }

                        // $('#lineChart').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart,300); },{accX: 0, accY: -155},'easeInCubic');
                        
                        <?php 

                            for($i=1;$i<21;$i++){
                                echo "function showLineChart".$i."(){ \n";
                                // echo "<br>";
                                echo "var ctx = document.getElementById('lineChartCanvas".$i."').getContext('2d');\n";
                                // echo "<br>";
                                echo "new Chart(ctx).Line(lineChartData".$i.",globalGraphSettings);}\n";
                                // echo "<br>";
                               
                                // echo "<br>";
                                echo "$('#lineChart".$i."').appear( function(){ $(this).css({ opacity: 1 });\n";
                                // echo "<br>";
                                echo "setTimeout(showLineChart".$i.",300); },{accX: 0, accY: -155},'easeInCubic');\n";
                                // echo "<br>";
    
                            }

                         ?>

                        

                        // function showLineChart2(){
                            
                        //     var ctx = document.getElementById("lineChartCanvas2").getContext("2d");
                        //     new Chart(ctx).Line(lineChartData2,globalGraphSettings);
                            
                        // }

                        // function showLineChart3(){
                            
                        //     var ctx = document.getElementById("lineChartCanvas3").getContext("2d");
                        //     new Chart(ctx).Line(lineChartData3,globalGraphSettings);
                            
                        // }   

                        // function showLineChart4(){
                            
                        //     var ctx = document.getElementById("lineChartCanvas4").getContext("2d");
                        //     new Chart(ctx).Line(lineChartData4,globalGraphSettings);
                            
                        // }   

                        // function showLineChart5(){
                            
                        //     var ctx = document.getElementById("lineChartCanvas5").getContext("2d");
                        //     new Chart(ctx).Line(lineChartData5,globalGraphSettings);
                            
                        // }  


                        // function showLineChart6(){
                            
                        //     var ctx = document.getElementById("lineChartCanvas5").getContext("2d");
                        //     new Chart(ctx).Line(lineChartData5,globalGraphSettings);
                            
                        // }  
                        

                       
                        // $('#lineChart2').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart2,300); },{accX: 0, accY: -155},'easeInCubic');
                        // $('#lineChart3').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart3,300); },{accX: 0, accY: -155},'easeInCubic');
                        // $('#lineChart4').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart4,300); },{accX: 0, accY: -155},'easeInCubic');
                        // $('#lineChart5').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart5,300); },{accX: 0, accY: -155},'easeInCubic');
                        //  $('#lineChart6').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart6,300); },{accX: 0, accY: -155},'easeInCubic');


                    });

                    </script>



                </div>

            </div>

        </section><!-- #content end -->

               
@stop