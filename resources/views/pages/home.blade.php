@extends('shared.header')

@section('page-content')

<!-- Slider
		============================================= -->
		<section id="slider" class="slider-parallax full-screen clearfix" style="transform: translate(0px, 0px); height: 505px; background-image: url('{{ URL::asset("public/img/green-bg.png") }}');">
		
			   <img  src="{{ URL::asset('public/img/whitelanding.png') }}" alt="Image" class="hidden-sm hidden-xs" data-style-lg="position: absolute; right: 0; bottom: 0; height: 200px;" data-style-md="position: absolute; right: 0; bottom: 0; height: 250px;">

			  <div class="container center ">
                    <img data-animate="rotateIn" data-delay="400" src="{{ URL::asset('public/img/splashscreen.png') }}" alt="Image" class="hidden-sm hidden-xs" data-style-lg="position: absolute; right: 0; bottom: 0; height: 600px;" data-style-md="position: absolute; right: 0; bottom: 0; height: 250px;">

                    <div class="vertical-middle  tleft ">
                        <div class="emphasis-title col-md-7 ">
                            <h1 class="title-section" data-animate="fadeInUp" data-delay="300">
                                <span class="text-rotater nocolor white" >
                                    <span  class="t-rotate  font-body  ">Forex Trading Simplified</span>
                                </span>
                            </h1>
                            <p data-animate="fadeInUp" data-delay="400" class=" lead white content-section">the easiest way to get your daily trading updates is now  in your hand, with Flux Forex App. Avalaible for free Download at  Google Play </p>

                             <a data-animate="fadeInUp"  href="#" >
                                <img src="{{ URL::asset('public/img/google-play-badge.png') }}" class="google-badge">
                             </a>
                        </div>
                       
                    </div>
                </div>
			

		</section>
		
		<!-- #slider end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap nopadding">

				<div id="section-about" class="center page-section nopadding">

					<div class="container clearfix">
						 <div class="col-md-6 nobottommargin center divcenter ">
	                        <h1 class="title-section2 topmargin-sm">
	                            <span class="text-rotater nocolor " >
	                                <span class="t-rotate   ">Notified. Trade. Repeat</span>
	                            </span>
	                        </h1>
	                        <p class="lead  content-section">We Know very well How's a trader's life goes by each day . Being notified by the current how of the market goes is what we note the most. Flux Forex serves Traders with every information they need directly to their pocket </p>

                   		 </div>
					</div>
					<div class="row topmargin-lg ">
	                    <div class="col-md-12">
	                        <div class="col-md-12 divcenter bg-section2" id="grad3" ></div>
	                        <div class="col-md-8  divcenter">
	                            <img class="" src="	{{ URL::asset('public/img/imgSection2-new.png') }}">
	                        </div>
	                    </div>       
               		</div>
               		 <!-- #content 2  -->
               		
               		<div class="row topmargin-sm"></div>
	                <div class="col-md-12 norightpadding ">
	                    <div class="col-md-5  nobottommargin topmargin-lg col_last">
	                        <h1 class="title-section topmargin-150" >
	                            <span class="text-rotater nocolor ">
	                                <span class="t-rotate font-body ">Detailed Readout</span>
	                            </span>
	                        </h1>
	                        <p class="lead  content-section">Flux Forex will only send a signal with 80% win rate. we will also give you daily market trends, so you can avoid unpredictable market and signal for no trade</p>
	                    </div>
	                     <div class="col-md-7 nobottommargin  norightmargin norightpadding vessel-imgsection3">
	                        <div >
	                            <img class="img-size" src=" {{ URL::asset('public/img/content/1.png') }}" data-animate="fadeInRight" >
	                        </div>
	                    </div>
	                </div>
                <!-- #content 2 end -->
	                 <div class="col-md-12 norightpadding noleftpadding">
	                     <div class="col-md-7 nobottommargin noleftpadding norightmargin norightpadding vessel-imgsection4">
	                         <div >
	                            <img class="img-size2" src=" {{ URL::asset('public/img/content/2.png') }}" data-animate="fadeInLeft"  >
	                        </div>
	                    </div>
	                    <div class="col-md-5  nobottommargin topmargin-lg col_last">
	                        <h1 class="title-section topmargin-150" >
	                            <span class="text-rotater nocolor " >
	                                <span class="t-rotate  ">Friendly Enviroment</span>
	                            </span>
	                        </h1>
	                        <p class="lead  content-section">Flux Forex Designed its signal as simple as it can be, so beginer trader can read it with easly. Every signal is broadcaster with pending order so you can follow along immedietly</p>

	                    </div>
	                </div>
	                <div class="col-md-12 norightpadding ">
	                    <div class="col-md-5  nobottommargin topmargin-md col_last">
	                        <h1 class="title-section topmargin-150" >
	                            <span class="text-rotater nocolor " >
	                                <span class="t-rotate  font-body">Reliable</span>
	                            </span>
	                        </h1>
	                        <p class="lead  content-section">Every signal we broadcast comes out under clear tranparency, and our customer support is always available for 24/7</p>
	                    </div>
	                     <div class="col-md-7 nobottommargin  norightmargin norightpadding vessel-imgsection3">
	                        <img class="img50" src="{{ URL::asset('public/img/content/3.png') }}" data-animate="fadeInUpBig" data-delay="400" > 
	                    </div>
	                    <div class="row " data-animate="fadeInUpBig">
	                         <div class="col-md-12 nopadding vessel-lastSession">
	                            <div class="skew">
	                                <div class="skew-wrap">
	                                    <div class="col-md-6 divcenter center">
	                                        <img data-animate="fadeInUp" data-delay="300" class="last-logo" src="{{ URL::asset('public/img/FluxLogowhite2.png') }}" alt="Canvas Logo">
	                                         <h1 data-animate="fadeInUp" data-delay="300" class="title-section center topmargin-sm" >
	                                            <span class="text-rotater nocolor white" >
	                                                <span class="t-rotate-word ">Get Flux Forex App</span>
	                                            </span>
	                                        </h1>
	                                         <a href="#" >
	                                            <img data-animate="fadeInUp" data-delay="300" src="{{ URL::asset('public/img/google-play-badge.png') }}" class="google-badge2">
	                                         </a>
	                                    </div>
	                                    
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                	</div>
				</div>
				<div class="section parallax nomargin dark" style="background-image: url('{{ URL::asset('public/images/page/testimonials.jpg') }}'); padding: 150px 0;" data-stellar-background-ratio="0.3">
					</div>
			
<!-- 
				<div id="section-services" class="page-section notoppadding">

				
					<div class="common-height clearfix">

					</div>


					<div class="section parallax nomargin dark" style="background-image: url('images/page/testimonials.jpg'); padding: 150px 0;" data-stellar-background-ratio="0.3">
					</div>

				</div>
 -->
			

			

			</div>

		</section><!-- #content end -->

		@include('shared.footer')

@stop