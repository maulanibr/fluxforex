@extends('shared.header-green')

@section('page-content') 
      <link rel="stylesheet" href=" {{ URL::asset('public/css/page/signal.css') }}" type="text/css" />
      <link rel="stylesheet" href="{{ URL::asset('public/css/page/stat.css') }}" type="text/css" />

      <?php function checkColor($profit){
        $operand = $profit[0];
        if($operand=='-'){
            echo 'red-text';

        }elseif($operand=='0'){
            echo 'grey-text';

        }else{
            echo 'green-text';
        }

      } 



      ?>
      

      <script>
        function selectCurrency(){

          var type = $('#currency_option').val();
          console.log(type);
          url = "{{ URL('/') }}/performance/"+type;
          urllain  = "https://www.google.com";
          console.log(encodeURI(url));

        
          window.location = encodeURI(url);
           return false;
          

        }

        function selectYearMonth(){
          var year = $('#year_select').val();
          var month = $('#month_select').val();
          
          url = "{{ URL('/') }}/performance/"+year+"/"+month;
          console.log(url);
          // urllain  = "https://www.google.com";
          console.log(encodeURI(url));

        
          window.location = encodeURI(url);
           return false;

        }
      </script>

      <!-- Slider
        ============================================= -->
        <section id="content" class="min-height520">

            <div class="content-wrap ">

                <div class="col-md-12 padding-side clearfix">
                  <form onsubmit="return selectYearMonth();" action="">
                    <div class="col-md-12 bottommargin-sm ">
                   <!-- 
                        <select  id="currency_option" name="template-contactform-service" class="sm-form-control searchbox diplayinline" required>
                            <option value=""> -- Select Currency Pair -- </option>
                            <option value="EUR - USD">EUR - USD</option>
                            <option value="USD - JPY">USD - JPY</option>
                            <option value="GBP - USD">GBP - USD</option>
                            <option value="USD - CHF">USD - CHF</option>
                            <option value="EUR - GBP">EUR - GBP</option>
                            <option value="EUR - JPY">EUR - JPY</option>
                            <option value="EUR - CHF">EUR - CHF</option>
                            <option value="AUD - USD">AUD - USD</option>
                            <option value="USD - CAD">USD - CAD</option>
                            <option value="NZD - USD">NZD - USD</option>
                            <option value="AUD - JPY">AUD - JPY</option>
                            <option value="NZD - JPY">NZD - JPY</option>
                            <option value="GBP - JPY">GBP - JPY</option>
                            <option value="CAD - JPY">CAD - JPY</option>
                            <option value="EUR - AUD">EUR - AUD</option>
                            <option value="GBP - AUD">GBP - AUD</option>
                            <option value="GBP - CHF">GBP - CHF</option>
                            <option value="EUR - CAD">EUR - CAD</option>
                            <option value="EUR - NZD">EUR - NZD</option>
                            <option value="GBP - CAD">GBP - CAD</option>
                        </select> -->
                        <select  id="year_select" name="year" class="sm-form-control searchbox diplayinline" required>
                            <option value=""> -- Select Year -- </option>
                            @forelse($yearList as $yl)
                            <option value="{{ $yl->years }}" <?php echo ($year==$yl->years) ? 'selected' : ''?> >{{ $yl->years }}</option>
                            @empty
                            <p>No Years Added</p>
                            @endforelse
                           
                        </select>
                        <select  id="month_select" name="month" class="sm-form-control searchbox diplayinline" required>
                            <option value=""> -- Select Month -- </option>
                            <option value="01" <?php echo ($month=='01') ? 'selected' : ''?>>January</option>
                            <option value="02" <?php echo ($month=='02') ? 'selected' : ''?>>February</option>
                            <option value="03" <?php echo ($month=='03') ? 'selected' : ''?>>March</option>
                            <option value="04" <?php echo ($month=='04') ? 'selected' : ''?>>April</option>
                            <option value="05" <?php echo ($month=='05') ? 'selected' : ''?>>May</option>
                            <option value="06" <?php echo ($month=='06') ? 'selected' : ''?>>June</option>
                            <option value="07" <?php echo ($month=='07') ? 'selected' : ''?>>July</option>
                            <option value="08" <?php echo ($month=='08') ? 'selected' : ''?>>August</option>
                            <option value="09" <?php echo ($month=='09') ? 'selected' : ''?>>September</option>
                            <option value="10" <?php echo ($month=='10') ? 'selected' : ''?>>Oktober</option>
                            <option value="11" <?php echo ($month=='11') ? 'selected' : ''?>>November</option>
                            <option value="12" <?php echo ($month=='12') ? 'selected' : ''?>>Desember</option>

                        </select>
                        <button type="submit" class="add-to-cart button nomargin diplayinline border-rads" style="background-color:#01c515; border-radius:5px; margin-left:15px !Important;">GO</button>
                    </div>
                  </form>
                    

                    <!-- Post Content
                    ============================================= -->
                   <div class="col-md-12" id="barChart" style="opacity: 0;">
                        <div id="grad1" class="col-md-12">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle  nobottommargin">TOTAL PROFIT</p>
                            </div>
                            <!-- <div  class="col-md-6 tright nopadding">
                                <i class=" icon icon-line2-bar-chart  nobottommargin"></i>
                                <i class=" icon icon-line2-pie-chart  nobottommargin"></i>
                            </div> -->
                        </div>
                         <div class="col-md-12 " style="border-bottom: solid 1px rgba(220,220,220,0.5) ;">
                            <div  class="col-md-6 left nopadding">
                                <p class="chart-tittle-month  nobottommargin">Monthly profit</p>
                            </div>
                            <div  class="col-md-6 tright nopadding">
                                <p class="chart-tittle-no nobottommargin"><?php echo $year ?></p>
                            </div>
                        </div>
                        <canvas id="barChartCanvas" width="1170" height="300"></canvas>
                    </div>
                  </div>


            </div>

        </section><!-- #content end -->

        <script type="text/javascript">
          jQuery(window).load( function(){
              
              var globalGraphSettings = {animation : Modernizr.canvas};
              
              var barChartData = {
                  labels : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Okt','Nov','Des'],
                  datasets : [
                      {
                          fillColor : "rgba(1, 197, 21,0.9)",
                          // strokeColor : "rgba(1, 197, 21,0.8) ",
                          // pointColor : "#01c515",
                          // pointStrokeColor : "#01c515",
                          data : [<?php foreach($result_stat as $uj){
                                      // $last = end($eurusd);
                                     
                                      

                                          echo (empty($uj) ? 0 : $uj).',';
                                      
                                  } ?>]
                      }
                     
                  ]
              };

              // function showLineChart1(){
                
              //   var ctx = document.getElementById("lineChartCanvas1").getContext("2d");
              //   new Chart(ctx).Line(lineChartData1,globalGraphSettings);
                
              // }
               function showBarChart(){
                            var ctx = document.getElementById("barChartCanvas").getContext("2d");
                            new Chart(ctx).Bar(barChartData,globalGraphSettings);
                        }

              $('#barChart').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showBarChart,300); },{accX: 0, accY: -155},'easeInCubic');
        });  
      </script>



    <!-- Slider
        ============================================= -->
        <section id="content" class="min-height520">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class=" nobottommargin clearfix">
                        <div class="table-responsive">
                          <table id="stat-table" class="table table-striped nobottommargin">
                            <thead id="stat-table-head">
                              <tr >
                                <th>Time</th>
                                <th>Currency</th>
                                <th>Mode</th>
                                <th>Entry</th>
                                <th>TP 1</th>
                                <th>TP 2</th>
                                <th>TP 3</th>
                                <th>SL</th>

                               
                               <!--  <th>Trigger Point</th> -->
                                <th>Closing Point</th>
                                <th>Profit Point</th>
                                 <!-- <th>Explanation</th> -->
                              </tr>
                            </thead>
                            <tbody>



                            @forelse($result as $key => $signals)
                              <tr>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->published}}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->currencyPair}}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->mode}}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->entryLevel }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->TP1 }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->TP2 }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->TP3 }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->SL }}</td>
                               
                                <!-- <td></td> -->
                               <!--  <td>{{ $signals->triggeredAt }}</td> -->
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->closedAt }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->profit }}</td>
                              </tr>
                            @empty
                              <p>No Signal Posted.</p>
                            @endforelse

                          
                            
                              
                            </tbody>
                          </table>
                        </div>

                    </div><!-- .postcontent end -->

                   

                </div>

            </div>

        </section>



@stop