@extends('shared.header-green')

@section('page-content') 
      <link rel="stylesheet" href=" {{ URL::asset('public/css/page/signal.css') }}" type="text/css" />

      <?php function checkColor($profit){
        $operand = $profit[0];
        if($operand=='-'){
            echo 'red-text';

        }elseif($operand=='0'){
            echo 'grey-text';

        }else{
            echo 'green-text';
        }

      } 



      ?>
    <!-- Slider
        ============================================= -->
        <section id="content" class="min-height520">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class=" nobottommargin clearfix">
                        <div class="table-responsive">
                          <table id="stat-table" class="table table-striped nobottommargin">
                            <thead id="stat-table-head">
                              <tr >
                                <th>Time</th>
                                <th>Currency</th>
                                <th>Mode</th>
                                <th>Entry</th>
                                <th>TP 1</th>
                                <th>TP 2</th>
                                <th>TP 3</th>
                                <th>SL</th>
                               
                               <!--  <th>Trigger Point</th> -->
                                <th>Closing Point</th>
                                <th>Profit Point</th>
                              </tr>
                            </thead>
                            <tbody>



                            @forelse($result as $key => $signals)
                              <tr>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->published}}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->currencyPair}}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->mode}}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->entryLevel }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->TP1 }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->TP2 }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->TP3 }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->SL }}</td>
                               
                                <!-- <td></td> -->
                               <!--  <td>{{ $signals->triggeredAt }}</td> -->
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->closedAt }}</td>
                                <td class="<?php echo checkColor($signals->profit) ?>">{{ $signals->profit }}</td>
                              </tr>
                            @empty
                              <p>No Signal Posted.</p>
                            @endforelse

                           <!--  <?php 
                            $count = 1;
                            
                            if(!empty($result)){   
                                foreach($result as $signals){ 

                            ?>
                              <tr>
                                <td><?php echo $count ?></td>
                                <td><?php echo $signals->currencyPair ?></td>
                                <td><?php echo $signals->mode ?></td>
                                <td><?php echo $signals->entryLevel ?></td>
                                <td><?php echo $signals->TP1 ?></td>
                                <td><?php echo $signals->TP2 ?></td>
                                <td><?php echo $signals->TP3 ?></td>
                                <td><?php echo $signals->SL ?></td>
                               
                               
                                <td><?php echo $signals->triggeredAt ?></td>
                                <td><?php echo $signals->closedAt ?></td>
                                <td><?php echo $signals->profit ?></td>
                              </tr>
                            <?php $count++;}}else{ ?>
                              <p>No Signal Posted.</p>
                            <?php } ?> -->
                            
                              
                            </tbody>
                          </table>
                        </div>

                    </div><!-- .postcontent end -->

                   

                </div>

            </div>

        </section>

@stop